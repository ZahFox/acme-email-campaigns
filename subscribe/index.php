<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>

</head>
<body>

<script type="text/javascript">
$(document).ready(function () {
    $('#submit').click(function() {
      checked = $("input[type=checkbox]:checked").length;

      if(!checked) {
        alert("You must check at least one checkbox.");
        return false;
      }

    });
});

</script>

<div class="container">
  <h2>Subscribe to ACME emails</h2>
  <form class="form-horizontal" action="index.php" method="post">
    <div class="form-group">
      <label class="control-label col-sm-2" for="email">Email:</label>
      <div class="col-sm-3">
        <input type="email" class="form-control" id="email" placeholder="Enter email" name="email" required>
      </div>
    </div>
    <label class="control-label col-sm-2" for="pwd">Interests:</label>
    <div class="form-group">        
      <div class="col-sm-offset-2 col-sm-10">
        <div class="checkbox">
          <label><input type="checkbox" name="interests[]" value="1"> Technology</label>
        </div>
        <div class="checkbox">
          <label><input type="checkbox" name="interests[]" value="2"> Sports</label>
        </div>
        <div class="checkbox">
          <label><input type="checkbox" name="interests[]" value="3"> Science</label>
        </div>
      </div>
    </div>
    <div class="form-group">        
      <div class="col-sm-offset-2 col-sm-10">
        <button type="submit" class="btn btn-default" id="submit">Submit</button>
      </div>
    </div>
  </form>
</div>

</body>
</html>

<?php
if(isset($_POST['email'])){
  //Subscribe

  include '../includes/config.inc.php';
  include '../includes/data_access/Data_Link.inc.php';
  // spl_autoload_register(function($class){
  //     include '../includes/'.$class.'.inc.php';
  // });

      // Initialize the database and data-access
      $db = Database_Link::getConnection();
      $subscriber_da = new Subscriber_Access($db);

      $result = $subscriber_da->createSubscriber($_POST['email'], $_POST['interests']);

      if ($result == true) {
           echo '<div class="alert alert-success">
              <strong>Success!</strong> Sucessfully subscribed with ' . $_POST['email'] . '
            </div>';
      } else {
           echo '<div class="alert alert-warning">
              <strong>Error!</strong> Failed to subscribe!
            </div>';
      }


}

?>
