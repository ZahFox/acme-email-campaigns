<?php
include('includes/config.inc.php');
include('includes/authentication/login_check.inc.php');

// Session Data
define('USER_TYPE', $_SESSION['user_type']); 
define('USER_ID', $_SESSION['user_id']);
//define('USER_TYPE', 'TEST'); 
//define('USER_ID', 1);

include('includes/data_access/Data_Link.inc.php');
?>

<style>
body {
    background-color: #ffc180;
    padding: 100px 0 0 0
    margin: 0;
}
nav {
    background-color: #b0e0e6;
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    padding: 0 0 0 2rem;
    margin: 0;
}
nav, nav > h1, nav > p {
    display: inline-block;
}
nav > p {
    margin: 1rem;
    text-align: right;
}
nav > h1 {
    color: #191970;
}
nav > p > a {
    text-decoration: none;
}
table {
    background-color: rgba(22, 22, 22, .42);
    margin: .5rem;
    border-radius: .42rem;
    border: .25rem solid #000000;
}
th {
    background-color: #ffdab3;
    border: .25rem solid #1a0d00;
}
td {
    background-color: #fff3e6;
    border: .2rem solid #331a00;
}
table, th, td {
     padding: .5rem;
     text-align: center;
}
.table-title {
    background-color: #cccab2;
}
</style>

<nav>
    <h1><?php echo(USER_TYPE . ' #' . USER_ID); ?> </h1>
    <p><a href="login.php"><button> Log Out</button></a></P>
</nav>
<br><br><br>

    <?php
        // makeDataTable('New Email Blast',
        // array('Subscriber ID', 'Email Blast ID', 'Subscriber Email', 'Email Hash'),
        // $campaign_da->createCampaignEmailBlast(1));

        // USER TESTING //
        // $user_da->createUser(array(
        //     'email' => 'employee02@acme.com',
        //     'password' => 'employee123'
        // ));


        // CAMPAIGN TESTING //

        $new_title = (substr(uniqid('', true), -6));
        $new_content = (substr(uniqid('', true), -6));
        $new_url = (substr(uniqid('', true), -6));
        makeDataTable("TEST: Update Campaign",
            array('ID', 'New Title', 'New Content', 'New URL', 'Result'),
            array( array(2, $new_title, $new_content, $new_url,
                $campaign_da->updateCampaign(2, $new_title, $new_content, $new_url)  ?
                '<p style="color: #006600; font-weight: bold;">PASSED</p>' : '<p style="color: #ff2828; font-weight: bold;">FAILED</p>')) );

        
        // $new_types = Database_Link::arrayToProcedureString(array(1,2,3));
        // makeDataTable("TEST: Create Campaign",
        //     array('Customer ID', 'User ID', 'Title', 'Content', 'URL', 'Types', 'Result'),
        //     array( array('1', '2', 'TEST666', 'TEST666', '555@666.gov', $new_types,
        //         $campaign_da->createCampaign(1, 2, 'TEST666', 'TEST666', '555@666.gov', array(1,2,3))  ?
        //         '<p style="color: #006600; font-weight: bold;">PASSED</p>' : '<p style="color: #ff2828; font-weight: bold;">FAILED</p>')) );


        makeDataTable('All Campaigns',
            array('ID', 'Customer', 'User', 'Title', 'Content', 'URL', 'Locked', 'Date Created'),
            $campaign_da->getCampaigns('all', 9000));

        makeDataTable('Your Campaigns',
            array('ID', 'Customer', 'User', 'Title', 'Content', 'URL', 'Locked', 'Date Created'),
            $campaign_da->getCampaigns('user', USER_ID));

        makeDataTable('Customer 1 Campaigns',
            array('ID', 'Customer', 'User', 'Title', 'Content', 'URL', 'Locked', 'Date Created'),
            $campaign_da->getCampaigns('customer', 1));

        makeDataTable('First Campaign',
            array('ID', 'Customer', 'User', 'Title', 'Content', 'URL', 'Locked', 'Date Created'),
            $campaign_da->getCampaigns('campaign', 1));

        makeDataTable('ALL Campaign Types',
            array('ID', 'Name'),
            $campaign_da->getCampaignTypes());

        makeDataTable('Email Blast 1 Campaign',
            array('ID', 'Customer', 'User', 'Title', 'Content', 'URL', 'Locked', 'Date Created'),
            $campaign_da->getCampaigns('blast', 1));


        // SUBSCIRBER TESTING // 

        // $new_email = (substr(uniqid('', true), -13)) . '@' . (substr(uniqid('', true), -13)) . '.com';
        // makeDataTable("TEST: Create Subscriber",
        //     array('Email', 'Result'),
        //     array( array($new_email,
        //         $subscriber_da->createSubscriber($new_email, array(1,2,3)) ?
        //         '<p style="color: #006600; font-weight: bold;">PASSED</p>' : '<p style="color: #ff2828; font-weight: bold;">FAILED</p>')) );


        // EMAIL TESTING //

        $email_id = 1;
        $hash = 'a61766b0a323576431ce0084b091ed2c3ec76063';

        $result = $email_da->getEmailOpened($email_id);
        makeDataTable("TEST: Check If Email ". $email_id ." Has Been Opened",
            array('ID', 'Opened'),
            array(array($email_id, $result[0] ? 'true' : 'false')));

        $result = $email_da->getLinkOpened($email_id);
        makeDataTable("TEST: Check If Email " . $email_id. "'s Link Has Been Opened",
            array('ID', 'Opened'),
            array(array($email_id, $result[0] ? 'true' : 'false')));

        $result = $email_da->setEmailOpened($email_id, $hash);
        makeDataTable("TEST: Open Email ". $email_id,
            array('ID', 'Hash', 'Success'),
            array(array($email_id, $hash, $result ?
            '<p style="color: #006600; font-weight: bold;">PASSED</p>' : '<p style="color: #ff2828; font-weight: bold;">FAILED</p>')) );

        $result = $email_da->setLinkOpened($email_id, $hash);
        makeDataTable("TEST: Open Email ". $email_id ."'s Link",
            array('ID', 'Hash', 'Success'),
            array(array($email_id, $hash, $result ?
            '<p style="color: #006600; font-weight: bold;">PASSED</p>' : '<p style="color: #ff2828; font-weight: bold;">FAILED</p>')) );



        // BLASTS
        makeDataTable("Campaign 1's Email Blasts",
            array('Email Blast ID', 'Campaign ID', 'Time Sent'),
                  $email_da->getEmailBlasts(1));
        
        makeDataTable("Email Blast 1's Emails",
            array('Email ID', 'Campaign ID', 'Subscriber ID', 'Email Opened', 'Link Opened', 'Email Blast ID'),
            $email_da->getEmailBlastEmails(1));
            


        // Close the connection to the database when the script is done
        Database_Link::closeConnection();
    ?>

<br>