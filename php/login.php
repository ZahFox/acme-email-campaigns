<?php # login.php #
# This script will allow authorized users to login and gain access to the application
include('includes/config.inc.php');
include('includes/data_access/Database_Link.inc.php');
include('includes/data_access/User_Access.inc.php');

$_SESSION = array();
session_destroy();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

	$email = $_POST['txtEmail'];
	$password = $_POST['txtPassword'];

	$db = Database_Link::getConnection();
	$user_da = new User_Access($db);
	$user = $user_da->login($email, $password);

	if ($user) {

		session_set_cookie_params(time()+1800, '/', SITE_DOMAIN, SSL, true);
		session_start();
		$_SESSION['created'] = time();
		$_SESSION['user_id'] = $user['user_id'];
		$_SESSION['user_type'] = $user['user_type'];
		header("Location: index.php");

	} else {

		$error = "<p style=\"color:red;\">Login Error: Please Try Again!</p>";

	}

	Database_Link::closeConnection();
}

?>

<form method="POST" action="login.php">
	<?php
	  if(isset($error)) {
		  echo($error);
	  }
	?>
	email: <input type="text" name="txtEmail" />
	<br>
	password: <input type="password" name="txtPassword" />
	<br>
	<input type="submit" value="Log In" />
</form>