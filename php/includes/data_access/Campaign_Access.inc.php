<?php
class Campaign_Access {

    private $connection;
    private static $getByTypes = array('campaign', 'customer', 'user', 'blast', 'all');

    /** ----------
	 * Constructor
	 * -----------		
	 */
	function __construct($db) {
		$this->connection = $db;
	}


    /** ------------------------------------------------------------------------------------
	* createCampaign :: This method is used to create a new campaign record in the database.
	* --------------------------------------------------------------------------------------
    * @param int $customer_id (this parameter is the id of the the customer that this campaign is for).
    * @param int $user_id (this parameter is the id of the the user that created the campaign).
    * @param string $title (this parameter contains the title of the new campaign).
    * @param string $content (this parametercontains the content of the new campaign).
    * @param string $url (this parameter contains the URL of the new campaign).
    * @param array $types (this parameter is an array of ids for each campaign type this campaign is linked to).
	* 
	* @return boolean Returns a boolean indicating whether or not the new campaign was created.
    */
    function createCampaign($customer_id, $user_id, $title, $content, $url, $types) {
        
        $customer_id = mysqli_real_escape_string($this->connection, $customer_id);
        $user_id = mysqli_real_escape_string($this->connection, $user_id);
        $title = mysqli_real_escape_string($this->connection, $title);
        $content = mysqli_real_escape_string($this->connection, $content);
        $url = mysqli_real_escape_string($this->connection, $url);
    
        $success = false;
        $pString = Database_Link::arrayToProcedureString($types);
        if ($pString) {
            $query = "CALL create_campaign('". $customer_id ."', '". $user_id ."', '". $title ."', '". $content ."', '". $url ."', '". $pString ."')";
            $result = mysqli_query($this->connection, $query);
    
            if ($result) {
                $row = mysqli_fetch_assoc($result);
                if ($row['success'] == 1) {
                    $success = true;
                }
                $result->close();
                $this->connection->next_result();
            }
        }
        return $success;
    }
        

    /** ---------------------------------------------------------------
	* getCampaigns :: This method is used to retrieve campaign records.
	* -----------------------------------------------------------------
	* @param string $by (this parameter must be one of the $getByTypes).
    * @param int $value (this parameter is an id associated with the type of $by, it is ignored for 'all').
	* 
	* @return array Returns a two-dimensional array of campaign records.
	*/
	function getCampaigns($by, $value) {

        $by = mysqli_real_escape_string($this->connection, $by);
        $value = mysqli_real_escape_string($this->connection, $value);

        if (in_array($by, static::$getByTypes)) {
            $query = "CALL fetch_campaign('". $by ."', '". $value ."')";
            
            $result = mysqli_query($this->connection, $query);
            if ($result) {
                $campaigns = array();
                while($row = mysqli_fetch_assoc($result)) {
                    $campaign = array(
                        'campaign_id' => htmlentities($row['campaign_id']),
                        'customer_id' => htmlentities($row['customer_id']),
                        'user_id' => htmlentities($row['user_id']),
                        'title' => htmlentities($row['title']),
                        'content' => htmlentities($row['content']),
                        'url' => htmlentities($row['url']),
                        'locked' => htmlentities($row['locked']),
                        'created' => htmlentities($row['created'])
                    );
                    $campaigns[] = $campaign;
                }
                $result->close();
                $this->connection->next_result();
                return $campaigns; 
            }
            return false;
        }     
    }


    /** ----------------------------------------------------------------------------------
	* getCampaignTypes :: This method is used to retrieve a record for every campaign type
	* ------------------------------------------------------------------------------------
	* 
	* @return array Returns a two-dimensional array of campaign_type records.
    */
    function getCampaignTypes() {

        $query = "CALL fetch_campaign_types()";
        $result = mysqli_query($this->connection, $query);

        if ($result) {
            $campaign_types = array();
            while($row = mysqli_fetch_assoc($result)) {
                $campaign_type = array(
                    'campaign_type_id' => htmlentities($row['campaign_type_id']),
                    'name' => htmlentities($row['name'])
                );
                $campaign_types[] = $campaign_type;
            }
            $result->close();
            $this->connection->next_result();
            return $campaign_types; 
        }
        return false;
    }


    /** ----------------------------------------------------------------------------------
    * getCampaignTypesById :: This method is used to retrieve a record for every Campaign_Type
    * associated with a particular Campaign.
	* ------------------------------------------------------------------------------------
	* 
	* @return array Returns a two-dimensional array of campaign_type records.
    */
    function getCampaignTypesById($id) {
        
        $query = "CALL fetch_campaign_types_by_id(". $id .")";
        $result = mysqli_query($this->connection, $query);

        if ($result) {
            $campaign_types = array();
            while($row = mysqli_fetch_assoc($result)) {
                $campaign_type = array(
                    'campaign_type_id' => htmlentities($row['campaign_type_id']),
                    'name' => htmlentities($row['name'])
                );
                $campaign_types[] = $campaign_type;
            }
            $result->close();
            $this->connection->next_result();
            return $campaign_types; 
        }
        return false;
    }


    /** -------------------------------------------------------------------
	* updateCampaign :: This method is used to update an unlocked campaign.
	* ---------------------------------------------------------------------
	* @param int $id (this is the id of the campaign that will be updated).
    * @param string $title (this parameter will be the updated title of the campaign).
    * @param string $content (this parameter will be the updated content of the campaign).
    * @param string $url (this parameter will be the updated url of the campaign).
    * @param array $types (this parameter is an array of ids for each campaign type this campaign is linked to).
	* 
	* @return bool Returns a boolean indicating where the procedure was successful or not.
	*/
	function updateCampaign($id, $customer_id, $title, $content, $url, $types) {

        $id = mysqli_real_escape_string($this->connection, $id);
        $customer_id = mysqli_real_escape_string($this->connection, $customer_id);
        $title = mysqli_real_escape_string($this->connection, $title);
        $content = mysqli_real_escape_string($this->connection, $content);
        $url = mysqli_real_escape_string($this->connection, $url);
        $pString = Database_Link::arrayToProcedureString($types);

        $query = "CALL update_campaign('". $id ."', '". $customer_id ."', '". $title ."', '". $content ."', '". $url ."', '". $pString ."')";
        $result = mysqli_query($this->connection, $query);

        if ($result) {
			$row = mysqli_fetch_assoc($result);

			if ($row['success'] == 1) {
                $result->close();
                $this->connection->next_result();
                return true;
            } 
            $result->close();
            $this->connection->next_result();  
		}
		return false;
    }


    /** ----------------------------------------------------------------------------------------------------------------------
	* createCampaignEmailBlast :: This method will be used to help send a campaign email blast and record the associated data.
	* ------------------------------------------------------------------------------------------------------------------------
    * @param int $id (this parameter is an id associated with the campaign that the email blast is associated with).
	* 
    * @return array Returns a two-dimensional array that contains the following fields:
    *               subscriber_id, email_blast_id, email, hash
    */
    function createCampaignEmailBlast($id) {

        $id = mysqli_real_escape_string($this->connection, $id);
        $query = "CALL create_campaign_email_blast(". $id .");";
        $result = mysqli_query($this->connection, $query); 

        if ($result) {
            $emails = array();
            while($row = mysqli_fetch_assoc($result)){
                $email = array(
                    'subscriber_id' => htmlentities($row['subscriber_id']),
                    'email_blast_id' => htmlentities($row['email_blast_id']),
                    'email' => htmlentities($row['email']),
                    'hash' => htmlentities($row['hash'])
                );
                $emails[] = $email;
            }
            $result->close();
            $this->connection->next_result();
            return $emails;
        }
        return false; 
    }


    /** --------------------------------------------------------------------------------
    * getCampaignBlastStats :: This method is used to retrieve stats from every email blast
    * associated with a particular campaign.
	* ----------------------------------------------------------------------------------
    * @param int $id (this is the campaign_id associated with the stats).
	* 
	* @return bool Returns a boolean indicating whether or not any stats were found.
	*/
	function getCampaignBlastStats($id) {
        
        $id = mysqli_real_escape_string($this->connection, $id);

        $success = false;
        $query = "CALL fetch_campaign_blast_stats(". $id .")";
        $result = mysqli_query($this->connection, $query);

        if ($result) {
            $stats = array();
            while($row = mysqli_fetch_assoc($result)) {
                $stats[] = array(
                    'blasts_sent' => htmlentities($row['blasts_sent']),
                    'emails_sent' => htmlentities($row['emails_sent']),
                    'emails_opened' => htmlentities($row['emails_opened']),
                    'links_opened' => htmlentities($row['links_opened'])
                );
            }
            $result->close();
            $this->connection->next_result();
            return $stats;
        }
        return false;
    }


    /** --------------------------------------------------------------------------------
    * getAllCampaignBlastStats :: This method is used to retrieve indivdual stats for
    * each email blast associated with a particular campaign.
	* ----------------------------------------------------------------------------------
    * @param int $id (this is the campaign_id associated with the stats).
	* 
	* @return bool Returns a boolean indicating whether or not any stats were found.
	*/
	function getAllCampaignBlastStats($id) {
        
        $id = mysqli_real_escape_string($this->connection, $id);

        $success = false;
        $query = "CALL fetch_all_campaign_blast_stats(". $id .")";
        $result = mysqli_query($this->connection, $query);

        if ($result) {
            $stats = array();
            while($row = mysqli_fetch_assoc($result)) {
                $stats[] = array(
                    'email_blast_id' => htmlentities($row['email_blast_id']),
                    'emails_sent' => htmlentities($row['emails_sent']),
                    'emails_opened' => htmlentities($row['emails_opened']),
                    'links_opened' => htmlentities($row['links_opened'])
                );
            }
            $result->close();
            $this->connection->next_result();
            return $stats;
        }
        return false;
    }
}