<?php
class Email_Access {

    private $connection;
    private static $getByTypes = array('campaign', 'all');

	/** ----------
	 * Constructor
	 * -----------		
	 */
	function __construct($db) {
		$this->connection = $db;
	}
    

    /** ----------------------------------------------------------------------------------
	* getEmailBlasts:: This method is used to retrieve Email_Blast records for a Campaign.
	* ------------------------------------------------------------------------------------
    * @param int $id (this parameter is an id associated with an existing Campaign).
	* 
	* @return array Returns a two-dimensional array of Email_Blast records.
	*/
	function getEmailBlasts($id) {

        $id = mysqli_real_escape_string($this->connection, $id);
        $query = "CALL fetch_email_campaign_blasts($id);";
        $result = mysqli_query($this->connection, $query);

        if ($result) {
            $blasts = array();
            while($row = mysqli_fetch_assoc($result)) {
                $blasts[] = array(
                    'email_blast_id' => htmlentities($row['email_blast_id']),
                    'campaign_id' => htmlentities($row['campaign_id']),
                    'time_sent' => htmlentities($row['time_sent'])
                );
            }
            $result->close();
            $this->connection->next_result();
            return $blasts;
        }

        return false;
    }


    /** ----------------------------------------------------------------------------------
	* getEmailBlastEmails :: This method is used to retrieve the Emails of an Email_Blast.
	* ------------------------------------------------------------------------------------
    * @param int $id (this parameter is an id associated with an existing Email_Blast).
	* 
	* @return array Returns a two-dimensional array of Email records.
	*/
	function getEmailBlastEmails($id) {
        
        $id = mysqli_real_escape_string($this->connection, $id);
        $query = "CALL fetch_email_blast_emails($id);";
        $result = mysqli_query($this->connection, $query);

        if ($result) {
            $emails = array();
            while($row = mysqli_fetch_assoc($result)) {
                $emails[] = array(
                    'email_id' => htmlentities($row['email_id']),
                    'campaign_id' => htmlentities($row['campaign_id']),
                    'subscriber_id' => htmlentities($row['subscriber_id']),
                    'email_opened' => htmlentities($row['email_opened']),
                    'link_opened' => htmlentities($row['link_opened']),
                    'email_blast_id' => htmlentities($row['email_blast_id'])
                );
            }
            $result->close();
            $this->connection->next_result();
            return $emails;
        }

        return false;
    }


    /** -------------------------------------------------------------------------
	* getEmailOpened :: This method is used to check if an email has been opened.
	* ---------------------------------------------------------------------------
	* @param int $id (this is the id of the email that will be examined).
	* 
	* @return array Returns an array with the `email_opened` field.
	*/
	function getEmailOpened($id) {
        
        $id = mysqli_real_escape_string($this->connection, $id);

        $query = "CALL check_email_opened('". $id ."')";
        $result = mysqli_query($this->connection, $query);

        if ($result && $result->num_rows == 1) {
            $row = mysqli_fetch_assoc($result);
            $result->close();
            $this->connection->next_result();
            return array(htmlentities($row['email_opened']));
        }
        return false;
    }


    /** -------------------------------------------------------------------------------
	* getLinkOpened :: This method is used to check if an email's link has been opened.
	* ---------------------------------------------------------------------------------
	* @param int $id (this is the id of the email that will be examined).
	* 
	* @return array Returns an array with the `link_opened` field.
	*/
	function getLinkOpened($id) {
        
        $id = mysqli_real_escape_string($this->connection, $id);

        $query = "CALL check_link_opened('". $id ."')";
        $result = mysqli_query($this->connection, $query);

        if ($result && $result->num_rows == 1) {
            $row = mysqli_fetch_assoc($result);
            $result->close();
            $this->connection->next_result();
            return array(htmlentities($row['link_opened']));
        }
        return false;
    }


    /** ------------------------------------------------------------------------------------
	* setEmailOpened :: This method is used to update an email's email_opened field to true.
	* --------------------------------------------------------------------------------------
    * @param int $id (this is the subscriber_id of the email that will be updated).
    * @param int $hash (this is the hash of the email that will be updated).
	* 
	* @return bool Returns a boolean indicating whether or not the field was updated.
	*/
	function setEmailOpened($id, $hash) {

        $id = mysqli_real_escape_string($this->connection, $id);
        $hash = mysqli_real_escape_string($this->connection, $hash);

        $success = false;
        $query = "CALL open_email(". $id .", '". $hash ."')";
        $result = mysqli_query($this->connection, $query);

        if ($result) {
			$row = mysqli_fetch_assoc($result);
			if ($row['success'] == 1) {
				$success = true;
			}
			$result->close();
			$this->connection->next_result();
		}
		return $success;
    }


    /** ----------------------------------------------------------------------------------
	* setLinkOpened :: This method is used to update an email's link_opened field to true.
	* ------------------------------------------------------------------------------------
    * @param int $id (this is the subscriber_id of the email link that will be updated).
    * @param int $hash (this is the hash of the email that will be updated).
	* 
	* @return bool Returns a boolean indicating whether or not the field was updated.
	*/
	function setLinkOpened($id, $hash) {

        $id = mysqli_real_escape_string($this->connection, $id);
        $hash = mysqli_real_escape_string($this->connection, $hash);

        $success = false;
        $query = "CALL open_link(". $id .", '". $hash ."')";
        $result = mysqli_query($this->connection, $query);

        if ($result) {
			$row = mysqli_fetch_assoc($result);
			if ($row['success'] == 1) {
				$success = true;
			}
			$result->close();
			$this->connection->next_result();
		}
		return $success;
    }


    /** --------------------------------------------------------------------------------
	* getEmailBlastStats :: This method is used to retrieve the stats of an email blast.
	* ----------------------------------------------------------------------------------
    * @param int $id (this is the email_blast_id associated with the stats).
	* 
	* @return bool Returns a boolean indicating whether or not any stats were found.
	*/
	function getEmailBlastStats($id) {
        
        $id = mysqli_real_escape_string($this->connection, $id);

        $success = false;
        $query = "CALL fetch_email_blast_stats(". $id .")";
        $result = mysqli_query($this->connection, $query);

        if ($result) {
            $stats = array();
            while($row = mysqli_fetch_assoc($result)) {
                $stats[] = array(
                    'emails_sent' => htmlentities($row['emails_sent']),
                    'emails_opened' => htmlentities($row['emails_opened']),
                    'links_opened' => htmlentities($row['links_opened'])
                );
            }
            $result->close();
            $this->connection->next_result();
            return $stats;
        }
        return false;
    }


    function handle_error($msg) {
		// how do we want to handle this? should we throw an exception
		// and let our custom EXCEPTION handler deal with it?????
		$stack_trace = print_r(debug_backtrace(), true);
		throw new Exception($msg . " - " . $stack_trace);
    }
}