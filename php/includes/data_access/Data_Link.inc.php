<?php
spl_autoload_register(function($class){
    include $class.'.inc.php';
});


// Initialize the database
$db = Database_Link::getConnection();

// Initialize the data access classes
$campaign_da = new Campaign_Access($db);
$subscriber_da = new Subscriber_Access($db);
$email_da = new Email_Access($db);
$user_da = new User_Access($db);



function makeDataTable($title, $headers, $data) {
    $table_string = '';
    $title_colspan = count($headers);

    // Make Title Row
    $table_string .= '<table> <tr> <th class="table-title" colspan="'. $title_colspan .'">' . $title . '</th></tr><tr>';

    // Make Headers Row
    foreach ($headers as $index => $value) {
        $table_string .= '<th>'. $value .'</th>';
    }
    $table_string .= '</tr>';

    // Make Data Rows
    foreach ($data as $index => $data_row) {
        $table_string .= '<tr>';
        foreach ($data_row as $key => $value) {
            $table_string .= "<td>$value</td>";
        }
        $table_string .= '</tr>';
    }

    $table_string .= '</table>';
    if ($table_string != '') {
        echo($table_string);
    }
}
