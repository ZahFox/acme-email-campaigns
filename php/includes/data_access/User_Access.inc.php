<?php
class User_Access {
	
	private $connection;

	/** ----------
	 * Constructor
	 * -----------		
	 */
	function __construct($db) {
		$this->connection = $db;
	}


	/** ----------------------------------------------------------
	* login :: Authenticates a User for accessing the application.
	* ------------------------------------------------------------
	* @param string $email
	* @param string $password
	* 
	* @return array If login is authenticated returns the user_id and user_type
	*/
	function login($email, $password) {
		$email = mysqli_real_escape_string($this->connection, $email);
		$password = mysqli_real_escape_string($this->connection, $password);

		$query = "CALL check_user_login('". $email ."')";
		
		$result = mysqli_query($this->connection, $query);

		if ($result) {
			$row = mysqli_fetch_assoc($result);
			$hash = htmlentities($row['password']);
			if (password_verify($password, $hash)) {
				$user = array();
				$user['user_id'] = htmlentities($row['user_id']);
				$user['user_type'] = htmlentities($row['user_type']);
				return $user;
			}
			$result->close();
			$this->connection->next_result();
		}
		return false;
	}


	/** ---------------------------------------------------------
	* updateLogin :: Updates an existing User in the User table.
	* -----------------------------------------------------------
	* @param array 		An array that has the following properties: 
	*					        `user_id`, `old_email`, `old_password`,
	*                 `new_email`, and `new_password`
	*
	* @return bool 	  Returns a boolean that indicates if the update was successful or not.
	* 					
	*/
	function updateLogin($id, $old_email, $old_password, $new_email, $new_password) {

		$id = mysqli_real_escape_string($this->connection, $id);
		$old_email = mysqli_real_escape_string($this->connection, $old_email);
		$old_password = mysqli_real_escape_string($this->connection, $old_password);
		$new_email = mysqli_real_escape_string($this->connection, $new_email);
		$new_password = password_hash(mysqli_real_escape_string($this->connection, $new_password), PASSWORD_DEFAULT);
		$success = false;
		// First, check for a User with the supplied old_email
		$queryCheck = "CALL check_user_login('". $old_email ."')";
		$result = mysqli_query($this->connection, $queryCheck);

		// If the User exists, compare the old_password with the one stored in the DB
		if ($result) {
			$row = mysqli_fetch_assoc($result);
			$hash = htmlentities($row['password']);
			// If the password matches, update the User Credentials

			if(password_verify($old_password, $hash)) {
				$result->close();
				$this->connection->next_result();
				$query = "CALL update_user_login('". $id ."', '". $old_email ."', '". $new_email ."', '". $new_password ."' );";
				$result2 = mysqli_query($this->connection, $query);
		
				if ($result2) {
					$row = mysqli_fetch_assoc($result2);
					if ($row['success'] == 1) {
						$success = true;
					}
					$result2->close();
					$this->connection->next_result();
				}
			}
		}

		return $success;
	}


	/** ---------------------------------------------------------
	* createUser :: Creates a new User record in the User table.
	* -----------------------------------------------------------
	* @param array 		An array that has the following properties: 
	*					        `email` and `password`
	*
	* @return bool 	  Returns a boolean to inidicate whether or not the new user was created .
	* 					
	*/
	function createUser ($email, $password) {

		$email = mysqli_real_escape_string($this->connection, $email);
		$password = password_hash(mysqli_real_escape_string($this->connection, $password), PASSWORD_DEFAULT);

		$success = false;
		$query = "CALL create_user('". $email ."', '". $password ."')";
		$result = mysqli_query($this->connection, $query);

		if ($result) {
			$row = mysqli_fetch_assoc($result);
			if ($row['success'] == 1) {
				$success = true;
			}
			$result->close();
			$this->connection->next_result();
		}
		return $success;
	}


	/** ---------------------------------------------------------
	* deleteUser :: Deletes a User record in the User table.
	* -----------------------------------------------------------
	* @param int $email		The `email` of the User to be deleted.
	*
	* @return bool 	Returns a boolean to inidicate whether or not the User was deleted.
	* 					
	*/
	function deleteUser ($email) {
		
		$email = mysqli_real_escape_string($this->connection, $email);

		$success = false;
		$query = "CALL delete_user('". $email ."')";
		$result = mysqli_query($this->connection, $query);

		if ($result) {
			$row = mysqli_fetch_assoc($result);
			
			if ($row['success'] == 1) {
				$success = true;
			}
			$result->close();
			$this->connection->next_result();
		}
		return $success;
	}


	/** ---------------------------------------------------------
	* getUser :: Retrieves a User record by user_id.
	* -----------------------------------------------------------
	* @param int $id		A user_id associated with a User
	*
	* @return array    An array that has the following properties: 
	*					`user_type` and `email`
	* 					
	*/
	function getUser ($id) {
		
		$id = mysqli_real_escape_string($this->connection, $id);
		
		$success = false;
		$query = "CALL fetch_user(". $id .")";
		$result = mysqli_query($this->connection, $query);

		if ($result) {
				$users = array();
				while($row = mysqli_fetch_assoc($result)) {
						$users[] = array(
								'user_type' => htmlentities($row['user_type']),
								'email' => htmlentities($row['email'])
						);
				}
				$result->close();
				$this->connection->next_result();
				return $users;
		}
		return false;
	}


	/** -----------------------------------
	* getUsers :: Retrieves all User records
	* -------------------------------------
	*
	* @return array    A two-dimensional array that has the following properties: 
	*					         `user_type` and `email`
	* 					
	*/
	function getUsers () {
		
		$success = false;
		$query = 'CALL fetch_users()';
		$result = mysqli_query($this->connection, $query);

		if ($result) {
			$users = array();
			while($row = mysqli_fetch_assoc($result)) {
					$users[] = array(
							'user_type' => htmlentities($row['user_type']),
							'email' => htmlentities($row['email'])
					);
			}
			$result->close();
			$this->connection->next_result();
			return $users;
		}
		return false;
	}


	/** --------------------------------------------
	* getCustomers :: Retrieves all Customer records
	* ----------------------------------------------
	*
	* @return array    A two-dimensional array that has the following properties: 
	*					         `customer_id` and `name`
	* 					
	*/
	function getCustomers () {
		
		$success = false;
		$query = 'CALL fetch_customers()';
		$result = mysqli_query($this->connection, $query);

		if ($result) {
				$stats = array();
				while($row = mysqli_fetch_assoc($result)) {
						$stats[] = array(
								'customer_id' => htmlentities($row['customer_id']),
								'name' => htmlentities($row['name'])
						);
				}
				$result->close();
				$this->connection->next_result();
				return $stats;
		}
		return false;
	}


	/** ----------------------------------------------------------------
	* deleteCustomer :: Deletes a Customer record in the Customer table.
	* ------------------------------------------------------------------
	* @param int $id  The `customer_id` of the Customer to be deleted.
	*
	* @return bool 	  Returns a boolean to inidicate whether or not the Customer was deleted.
	* 					
	*/
	function deleteCustomer ($id) {
		
		$id = mysqli_real_escape_string($this->connection, $id);

		$success = false;
		$query = "CALL delete_customer(". $id .")";
		$result = mysqli_query($this->connection, $query);

		if ($result) {
			$row = mysqli_fetch_assoc($result);
			if ($row['success'] == 1) {
				$success = true;
			}
			$result->close();
			$this->connection->next_result();
		}
		return $success;
	}


	/** ----------------------------------------------------------------
	* updateCustomer :: Updates a Customer record in the Customer table.
	* ------------------------------------------------------------------
	* @param int    $id 		The `customer_id` of the Customer to be updated.
	* @param string $name		The `name` of the Customer to be updated.
	*
	* @return bool 	Returns a boolean to inidicate whether or not the Customer was updated.
	* 					
	*/
	function updateCustomer ($id, $name) {
		
		$id = mysqli_real_escape_string($this->connection, $id);
		$name = mysqli_real_escape_string($this->connection, $name);

		$success = false;
		$query = "CALL update_customer(". $id .", '". $name ."')";
		$result = mysqli_query($this->connection, $query);

		if ($result) {
			$row = mysqli_fetch_assoc($result);
			if ($row['success'] == 1) {
				$success = true;
			}
			$result->close();
			$this->connection->next_result();
		}
		return $success;
	}


	/** --------------------------------------------------------------------
	* createCustomer :: Creates a new Customer record in the Customer table.
	* ----------------------------------------------------------------------
	* @param string 		$name  The `name` of the new customer
	*
	* @return bool 	  Returns a boolean to inidicate whether or not the new Customer was created .
	* 					
	*/
	function createCustomer ($name) {
		
				$name = mysqli_real_escape_string($this->connection, $name);
		
				$success = false;
				$query = "CALL 	create_customer('". $name ."')";
				$result = mysqli_query($this->connection, $query);
		
				if ($result) {
					$row = mysqli_fetch_assoc($result);
					if ($row['success'] == 1) {
						$success = true;
					}
					$result->close();
					$this->connection->next_result();
				}
				return $success;
	}


} // END OF CLASS