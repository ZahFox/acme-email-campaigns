<?php
class Subscriber_Access{

    private $connection;
    
	/** ----------
	 * Constructor
	 * -----------		
	 */
	function __construct($db) {
		$this->connection = $db;
	}


    /** ---------------------------------------------------------------------------------------------
	* createSubscriber :: This method will be used to create a new subscriber record in the database.
	* -----------------------------------------------------------------------------------------------
    * @param string $email (this parameter is an id associated with the campaign that the email blast is associated with).
    * @param array $types (this parameter is an array of ids associated with the campaign types this subscriber is linked to).
	* 
	* @return boolean Returns a boolean indicating whether or not the new subscriber was created.
    */
    function createSubscriber($email, $types) {

        $email = mysqli_real_escape_string($this->connection, $email);
    
        $pString = Database_Link::arrayToProcedureString($types);
        if ($pString) {
            $query = "CALL create_subscriber('". $email ."', '". $pString ."');";
            $result = mysqli_query($this->connection, $query);
    
            if ($result) {
                $row = mysqli_fetch_assoc($result);
    
                if ($row['success'] == 1) {
                    $result->close();
                    $this->connection->next_result();
                    return true;
                }
                $result->close();
                $this->connection->next_result();
                return false;
            }
            return false;
        }
        return false;
    }


    /** ------------------------------------------------------------------
	* getSubscribers :: Retrieves a Subscriber records that are paginated.
	* --------------------------------------------------------------------
    * @param int $start		The starting index of records to be returned.
    * @param int $max    	The maximum amount of records to be returned.
	*
	* @return array    A tow-dimensional array that has the following properties: 
	*				   `subscriber_id` and `email`
	* 					
	*/
	function getSubscribers ($start, $max) {
		
        $start = mysqli_real_escape_string($this->connection, $start);
        $max = mysqli_real_escape_string($this->connection, $max);
		
		$success = false;
		$query = "CALL fetch_subscribers('". $start ."', '". $max ."')";
		$result = mysqli_query($this->connection, $query);

		if ($result) {
				$subscribers = array();
				while($row = mysqli_fetch_assoc($result)) {
						$subscribers[] = array(
								'subscriber_id' => htmlentities($row['subscriber_id']),
								'email' => htmlentities($row['email'])
						);
				}
				$result->close();
				$this->connection->next_result();
				return $subscribers;
		}
		return false;
    }
    

    /** ----------------------------------------------------------------------
	* deleteSubscriber :: Deletes a Subscriber record in the Subscriber table.
	* ------------------------------------------------------------------------
	* @param int $id    The `subscriber_id` of the Subscriber to be deleted.
	*
	* @return bool 	Returns a boolean to inidicate whether or not the Subscriber was deleted.
	* 					
	*/
	function deleteSubscriber ($id) {
		
		$id = mysqli_real_escape_string($this->connection, $id);

		$success = false;
		$query = "CALL delete_subscriber('". $id ."')";
		$result = mysqli_query($this->connection, $query);

		if ($result) {
			$row = mysqli_fetch_assoc($result);
			if ($row['success'] == 1) {
				$success = true;
			}
			$result->close();
			$this->connection->next_result();
		}
		return $success;
	}


    function handle_error($msg) {
		// how do we want to handle this? should we throw an exception
		// and let our custom EXCEPTION handler deal with it?????
		$stack_trace = print_r(debug_backtrace(), true);
		throw new Exception($msg . " - " . $stack_trace);
    }
}