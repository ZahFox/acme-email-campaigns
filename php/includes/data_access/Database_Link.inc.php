<?php # Database_Link is a Singleton class for global database access. #
# This is deemed appropriate because this application will only require one databse. #
# The mysqli connection parameters must be configfured in the config.inc.php file. #

class Database_Link{

    private static $instance;
    private static $connection;

    /** ------------------
	 * Private Constructor
	 * -------------------	
	 */
    final private function __construct() {
        self::$connection = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
        if (!self::$connection) {
            die(mysqli_connect_error());
        }
    }

    // Disabled Methods
    final private function __clone() {}
    final private function __wakeup() {}


    /** -----------------------------------------------------------------------
	* getConnection :: This method is used to retrieve the database connection.
	* -------------------------------------------------------------------------
	* 
	* @return mysqli Returns a mysqli database connection.
	*/
    final public static function getConnection() {
        if (!self::$instance) {
            self::$instance = new self();
        }
        return self::$connection;
    }


    /** ----------------------------------------------------------------------------------
	* closeConnection :: This should be called when database access in no longer required.
	* ------------------------------------------------------------------------------------
	* 
	* @return bool Returns true after the database connection is closed.
	*/
    final public static function closeConnection() {
        mysqli_close(self::$connection);
        return true;
    }


    /** ------------------------------------------------------------------------------------------------
	* arrayToProcedureString :: This method formats an array into a string for use in mysql procedures.
    * --------------------------------------------------------------------------------------------------
    * @param array $array (this parameter may be any one-dimensional array of values).
	* 
	* @return string Returns a string formatted for use in mysql stored procedures.
	*/
    final public static function arrayToProcedureString($array) {
        $pString = '';
        for ($i = 0; $i < count($array); $i++) {
            $array[$i] = (mysqli_real_escape_string(self::$connection, $array[$i]));
            $pString .= $array[$i] . ',';
        }
        if ($pString != '') {
            $pString .= '#';
            return $pString;
        } else {
            return false;
        }
    }
}