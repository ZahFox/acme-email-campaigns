<?php # login_check.inc.php #
# This script will check the values for three session variables: created, user_type and user_id
# If the criteria is not met, the user is redirected back to the login page

function indexCheck() {
	if( isset($_SESSION['created']) && ( (time() - $_SESSION['created']) <= SESSION_MAX_TIME) ) {
		
			if( isset($_SESSION['user_type']) && isset($_SESSION['user_id']) &&
				($_SESSION['user_type'] == 'Admin' || $_SESSION['user_type'] == 'Employee')
				&& ($_SESSION['user_id'] != -1) ) {
				 // ACCESS GRANTED
			} else {
				// REDIRECT TO LOGIN PAGE
				header("Location: login.html");
			}
		
		} else {
			// REDIRECT TO LOGIN PAGE
			header("Location: login.html");
		}
}

function apiCheck() {
	if( isset($_SESSION['created']) && ( (time() - $_SESSION['created']) <= SESSION_MAX_TIME) ) {
		
			if( isset($_SESSION['user_type']) && isset($_SESSION['user_id']) &&
				($_SESSION['user_type'] == 'Admin' || $_SESSION['user_type'] == 'Employee')
				&& ($_SESSION['user_id'] != -1) ) {
				 // ACCESS GRANTED
				 return true;
			} else {
				// REDIRECT TO LOGIN PAGE
				return false;
			}
		
		} else {
			// REDIRECT TO LOGIN PAGE
			return false;
		}
}


