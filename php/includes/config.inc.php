<?php # config.inc.php #
# This is the main configuration file for the application
define('SESSION_MAX_TIME', 1800); // Sessions may only last 30 minutes
define('SESSION_NAME', 'vG55s_$32si'); // Custom name for session cookie

// CONFIGURE HOST ENVIRONMENT
if ($_SERVER['SERVER_NAME'] == 'localhost') {
	// DEV ENVIRONMENT SETTINGS
	define('DEBUG_MODE', true);
	define('DB_HOST', 'localhost');
	define('DB_USER', 'root');
	define('DB_PASS', '_DivVb76JJesfy54fdghDFs12');
	define('DB_NAME', 'zahfoxco_ACME_Email');
	define('SITE_ADMIN_EMAIL', 'admin@acme.com');
	define('SITE_DOMAIN', $_SERVER['SERVER_NAME']);
	define('ROOT_DIR', '/');
	define('SSL', false);
} else {
	// PRODUCTION SETTINGS
	define('DEBUG_MODE', true); 
	define('DB_HOST', 'www.zahfox.com');
	define('DB_USER', 'zahfoxco_user');
	define('DB_PASS', 'xT31ssui2');
	define('DB_NAME', 'zahfoxco_ACME_Email');
	define('SITE_ADMIN_EMAIL', 'admin@acme.zahfox.com');
	define('SITE_DOMAIN', $_SERVER['SERVER_NAME']);
	define('ROOT_DIR', '/acme');
	define('SSL', true);
}

// if we are in debug mode then display all errors and set error reporting to all 
if (DEBUG_MODE) {
	// turn on error messages
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);
}


// Manually configure SECURE PHP INI settings..
ini_set('session.cookie_lifetime', SESSION_MAX_TIME); 
ini_set('session.gc_maxlifetime', SESSION_MAX_TIME);
ini_set('session.name', SESSION_NAME);
ini_set('session.cookie_path', ROOT_DIR);
ini_set('session.use_strict_mode', 'On');
ini_set('session.cookie_httponly', 'On');
ini_set('session.sid_length', '48');
ini_set('session.hash_bits_per_character', '6');
ini_set('session.hash_function', 'sha256');
if (SSL) {
	ini_set('session.cookie_secure', 'On');
} else {
	ini_set('session.cookie_secure', 'Off');
}
session_set_cookie_params(time()+1800, '/', SITE_DOMAIN, SSL, true);
session_start();


// set up custom error handling
require('custom_error_handler.inc.php');