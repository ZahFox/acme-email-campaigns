<?php
include('includes/config.inc.php');
include('includes/authentication/login_check.inc.php');
indexCheck(); 
?>

<!DOCTYPE html><html><head><meta charset=utf-8><meta name=viewport content="width=device-width,initial-scale=1"><title>email-campaigns</title><link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons" rel=stylesheet type=text/css><link href=/static/css/app.16d993c6ce69dcb393fa392e658b357a.css rel=stylesheet></head><body><div id=app></div><script type=text/javascript src=/static/js/manifest.e17866a4399417f95533.js></script><script type=text/javascript src=/static/js/vendor.c1b0ea3c47f13ab634cf.js></script><script type=text/javascript src=/static/js/app.ed7dbb6743774804c669.js></script></body></html>