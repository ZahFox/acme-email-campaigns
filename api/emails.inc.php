<?php
class EmailAPI extends API
{
    public function __construct($request, $origin) {
        parent::__construct($request);
    }
    /*
        EMAILS ENDPOINTS

        /geb/ - GET EMAIL BLASTS
        /geo/ - GET EMAIL OPENED
        /gli/ - GET LINK OPENED
        /seo/ - SET EMAIL OPENED
        /slo/ - SET LINK OPENED
    */
     protected function emails() {
        global $email_da;

        switch ($this->verb) {
            /**
             * /geb/ GET EMAIL BLASTS
             * @api {get} /emails/geb/:id

             * @apiParam {id} - id associated with a Campaign record.
             *
             * @apiSuccess Returns a two-dimensional array of Email_Blast records.
             */
            case "geb":
                if ($this->authorized === true) {
                    if ($this->method == 'GET') {
                        return $email_da->getEmailBlasts($this->args[0]);
                    } 
                    else {
                        return array( 'success' => false,
                                      'message' => 'This Route Requires a GET Request..' );
                    }
                } else {
                        return array( 'success' => false,
                                      'message' => 'Unauthorized Access!' );
                }
                break;

            /**
             * /geo/ GET EMAIL OPENED
             * @api {get} /emails/geo/:id
             *
             * @apiParam {ID} - id of the email that will be examined
             *
             * @apiSuccess Returns an array with the `email_opened` field.
             */
            case "geo":
                if ($this->authorized === true) {
                    if ($this->method == 'GET') {
                        return $email_da->getEmailOpened($this->args[0]);
                    } 
                    else {
                        return array( 'success' => false,
                                      'message' => 'This Route Requires a GET Request..' );
                    }
                } else {
                        return array( 'success' => false,
                                      'message' => 'Unauthorized Access!' );
                }
                break;

            /**
             * /glo/ GET LINK OPENED
             * @api {get} /emails/glo/:id
             *
             * @apiParam {ID} - id of the email that will be examined
             *
             * @apiSuccess Returns an array with the `link_opened` field.
             */
            case "glo":
                if ($this->authorized === true) {
                    if ($this->method == 'GET') {
                        return $email_da->getLinkOpened($this->args[0]);
                    } 
                    else {
                        return array( 'success' => false,
                                      'message' => 'This Route Requires a GET Request..' );
                    }
                } else {
                    return array( 'success' => false,
                                  'message' => 'Unauthorized Access!' );
                }
                break;

            /**
             * /gebs/ GET EMAIL BLAST STATS
             * @api {get} /emails/gebs/:id
             *
             * @apiParam {ID} - id of the email blast that will be examined
             *
             * @apiSuccess Returns an array with the `emails_sent`, `emails_opened`,
             * and `links_opened` fields.
             */
            case 'gebs':
                if ($this->authorized === true) {
                    if ($this->method == 'GET') {
                        return $email_da->getEmailBlastStats($this->args[0]);
                    }
                    else {
                        return array( 'success' => false,
                                      'message' => 'This Route Requires a GET Request..' );
                    }
                } else {
                    return array( 'success' => false,
                                  'message' => 'Unauthorized Access!' );
                }
                break;

            /**
             * /seo/ SET EMAIL OPENED
             * @api {put} /emails/seo/
             *
             * -- PUT BODY --
             * @apiParam {ID} - subscriber_id of the email that will be updated
             * @apiParam {HASH} - hash of the email that will be updated
             *
             * @apiSuccess Returns a boolean indicating whether or not the field was updated.
             */
            case "seo":
                if ($this->authorized === true) {
                    if ($this->method == 'PUT') {
                        $put = fopen('php://input', 'r');
                        $putDATA = '';

                        while($data = fread($put, 1024))
                            $putDATA .= $data;
                        fclose($put);
                        parse_str($putDATA, $putDATA);

                        $output = "ID: " . $putDATA['id'] . " | HASH: " . $putDATA['hash'];
                        file_put_contents("test.txt", $output);

                        return $email_da->setEmailOpened($putDATA['id'], $putDATA['hash']);
                    } 
                    else {
                        return array( 'success' => false,
                                      'message' => 'This Route Requires a PUT Request..' );
                    }
                } else {
                    return array( 'success' => false,
                                  'message' => 'Unauthorized Access!' );
                }
                break;

            /**
             * /slo/ SET LINK OPENED
             * @api {put} /emails/slo/
             *
             * -- PUT BODY --
             * @apiParam {ID} - subscriber_id of the email that will be updated
             * @apiParam {HASH} - hash of the email that will be updated
             *
             * @apiSuccess Returns a boolean indicating whether or not the field was updated.
             */
            case "slo":
                if ($this->authorized === true) {
                    if ($this->method == 'PUT') {
                        parse_str(file_get_contents('php://input'), $_PUT);
    
                        return $email_da->setLinkOpened($_PUT['id'], $_PUT['hash']);
                    } 
                    else {
                        return array( 'success' => false,
                                      'message' => 'This Route Requires a PUT Request..' );
                    }
                } else {
                    return array( 'success' => false,
                                  'message' => 'Unauthorized Access!' );
                }
                break;

            default:
                return array( 'success' => false,
                              'message' => 'Unknown Route' );
                break;
        }   
     }
}

