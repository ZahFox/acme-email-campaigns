<?php

class SubscriberAPI extends API
{

    public function __construct($request, $origin) {
        parent::__construct($request);
    }
    /*
    SUBSCRIBERS ENDPOINTS

    /subscribers

    */
    protected function subscribers() {
        global $subscriber_da;

        switch ($this->verb) {


            case 'gs':
                if ($this->authorized === true && $this->admin === true) {
                    if ($this->method == 'GET') {
                        return $subscriber_da->getSubscribers($this->args[0], $this->args[1]);
                    } 
                    else {
                        return array( 'success' => false,
                                    'message' => 'This Route Requires a GET Request..' );
                    }
                } else {
                    return array( 'success' => false,
                                'message' => 'Unauthorized Access!' );
                }
            break;


            case "ds":
                if ($this->authorized === true && $this->admin === true) {
                    if ($this->method == 'DELETE') {
                        $success = $subscriber_da->deleteSubscriber($this->args[0]);
                        if ($success === true) {
                            return array('success' => true,
                                         'message' => 'Subscriber Successfully Deleted!');
                        } else {
                            return array('success' => false,
                                         'message' => 'Subscriber was not Deleted!');
                        }
                    } 
                    else {
                        return array( 'success' => false,
                                    'message' => 'This Route Requires a DELETE Request..' );
                    }
                } else {
                    return array( 'success' => false,
                                'message' => 'Unauthorized Access!' );
                }
            break;


            case "cs":
                if ($this->method == 'POST') {
                    $success = $subscriber_da->createSubscriber($this->data['email'], $this->data['types']);
                    if ($success === true) {
                        return array('success' => true,
                                     'message' => 'Subscriber Successfully Created!');
                    } else {
                        return array('success' => false,
                                     'message' => 'Subscriber was not Created!');
                    }
                } 
                else {
                    return array( 'success' => false,
                                'message' => 'This Route Requires a POST Request..' );
                }

            break;

            default:

                return array( 'success' => false,
                            'message' => 'Unknown Route' );
            break;

        }
    }



}