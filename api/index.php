<?php
require '../includes/config.inc.php';
require '../includes/authentication/login_check.inc.php';

// Check to see if the client request holds a valid session cookie
$authenticated = apiCheck();
$authorized = false;
$admin = true; // CHANGE THIS FOR PROD!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

require '../includes/data_access/Data_Link.inc.php';

//define('USER_ID', 1); // DELETE THIS FOR PROD!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

if ($authenticated === true) {
    define('USER_ID', $_SESSION['user_id']);
    if($_SESSION['user_type'] === 'Admin') {
        $admin = true;
    }
}
if (defined('USER_ID')) {
    $authorized = true;
}


// Requests from the same server don't have a HTTP_ORIGIN header
if (!array_key_exists('HTTP_ORIGIN', $_SERVER)) {
    $_SERVER['HTTP_ORIGIN'] = $_SERVER['SERVER_NAME'];
}


// CHECK FOR LOGIN
$args = explode('/', rtrim($_REQUEST['request'], '/'));

if ($args[0] === 'login') {
    $method = $_SERVER['REQUEST_METHOD'];
    if ($method == 'POST') {
        $data = json_decode(file_get_contents("php://input"), true);
        // Remove the previous session
        $_SESSION = array();
        session_destroy();
        $email = $data['email'];
        $password = $data['password'];
        $user = $user_da->login($email, $password);

        if ($user) {
            session_set_cookie_params(time()+1800, '/', SITE_DOMAIN, SSL, true);
            session_start();
            $_SESSION['created'] = time();
            $_SESSION['user_id'] = $user['user_id'];
            $_SESSION['user_type'] = $user['user_type'];
            $msg = array('success' => true,
                         'message' => 'Login Successful!');
        } else {
            $msg = array('success' => false);
        }
    } 
    else {
        $msg = array( 'success' => false,
                      'message' => 'This Route Requires POST Request..' );
    }
    header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
    header("Cache-Control: no-store, no-cache, must-revalidate");
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");
    header("Access-Control-Allow-Orgin: *");
    header("Access-Control-Allow-Methods: *");
    header("Content-Type: application/json");

    echo json_encode($msg);

} else {

    // If not the login check all other endpoints
    require 'abstract_api.php';
    
    //CAMPAIGNS ENDPOINT
     if($args[0] == 'campaigns') {
        require 'campaigns.inc.php';
        try {
            $CampaignAPI = new CampaignAPI($_REQUEST['request'], $_SERVER['HTTP_ORIGIN']);
            echo $CampaignAPI->processAPI($authorized, $admin);
        } 
        catch (Exception $e) {
            echo json_encode(Array('error' => $e->getMessage()));
        }
    }
    
    //EMAILS ENDPOINT
    else if($args[0] == 'emails') {
        require 'emails.inc.php';
        try {
            $EmailAPI = new EmailAPI($_REQUEST['request'], $_SERVER['HTTP_ORIGIN']);
            echo $EmailAPI->processAPI($authorized, $admin);
        } 
        catch (Exception $e) {
            echo json_encode(Array('error' => $e->getMessage()));
        }
    }
    
    //USERS ENDPOINT
    else if($args[0] == 'users') {
        require 'users.inc.php';
        try {
            $UserAPI = new UserAPI($_REQUEST['request'], $_SERVER['HTTP_ORIGIN']);
            echo $UserAPI->processAPI($authorized, $admin);
        } 
        catch (Exception $e) {
            echo json_encode(Array('error' => $e->getMessage()));
        }
    }
    
    //SUBSCRIBERS ENDPOINT
    else if($args[0] == 'subscribers') {
        require 'subscribers.inc.php';
        try {
            $SubscriberAPI = new SubscriberAPI($_REQUEST['request'], $_SERVER['HTTP_ORIGIN']);
            echo $SubscriberAPI->processAPI($authorized, $admin);
        } 
        catch (Exception $e) {
            echo json_encode(Array('error' => $e->getMessage()));
        }
    }
    
    
    else {
        echo "error";
    }

}




?>