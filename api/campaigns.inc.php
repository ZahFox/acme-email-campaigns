<?php
class CampaignAPI extends API
{
    public function __construct($request, $origin) {
        parent::__construct($request);
    }
    /*
        CAMPAIGNS ENDPOINTS

        /cc/ - CREATE CAMPAIGN
        /gc/ - GET CAMPAIGN
        /gct/ - GET CAMPAIGN TYPES
        /uc/ - UPDATE CAMPAIGN
        /cceb/ - CREATE CAMPAIGN EMAIL BLAST
    */
     protected function campaigns() {
        global $campaign_da;

        switch ($this->verb) {

            /**
             * /cc/ CREATE CAMPAIGN
             * @api {post} /campaigns/cc/
             *
             * -- POST BODY --
             * @apiParam {customer_id} - id of the the customer that this campaign is for
             * @apiParam {user_id} - id of the the user that created the campaign -- AUTO GRABBED
             * @apiParam {title} - title of the new campaign
             * @apiParam {content} - content of the new campaign
             * @apiParam {url} - URL of the new campaign
             * @apiParam {types} - ids for each campaign type this campaign is linked to
             *
             * @apiSuccess Returns a boolean indicating whether or not the new campaign was created.
             */
            case "cc":
                if ($this->authorized === true) {
                    if ($this->method == 'POST') {
                        // return $campaign_da->createCampaign($_POST['customer_id'], USER_ID, $_POST['title'], $_POST['content'], $_POST['url'], $_POST['types']);
                        return $campaign_da->createCampaign($this->data['customer_id'], USER_ID, $this->data['title'],
                                                    $this->data['content'], $this->data['url'], $this->data['types']);
                    } 
                    else {
                        return array( 'success' => false,
                                      'message' => 'This Route Requires a POST Request..' );
                    }
                } else {
                    return array( 'success' => false,
                                  'message' => 'Unauthorized Access!' );
                }
                break;

            /**
             * /gc/ GET CAMPAIGN
             * @api {get} /campaigns/gc/:by/:value
             *
             * @apiParam {BY} 'campaigns', 'user', 'type', 'blast'
             * @apiParam {VALUE} an id associated with the type of $by
             *
             * @apiSuccess Returns a two-dimensional array of campaign records.
             */
            case "gc":
                if ($this->authorized === true) {
                    if ($this->method == 'GET') {
                        global $campaign_da;
                        if ($this->args[0] == "all") {
                            return $campaign_da->getCampaigns($this->args[0], -1);
                        } else if ($this->args[0] == "user") {
                            return $campaign_da->getCampaigns($this->args[0], USER_ID);
                        } else {
                            return $campaign_da->getCampaigns($this->args[0], $this->args[1]);
                        }   
                    } 
                    else {
                        return array( 'success' => false,
                                      'message' => 'This Route Requires a GET Request..' );
                    }
                } else {
                    return array( 'success' => false,
                                  'message' => 'Unauthorized Access!' );
                }
                break;

            /**
             * /gct/ GET CAMPAIGN TYPES
             * @api {get} /campaigns/gct/
             *
             *
             * @apiSuccess Returns a two-dimensional array of campaign_type records.
             */
            case "gct":
                if ($this->authorized === true) {
                    if ($this->method == 'GET') {
                        return $campaign_da->getCampaignTypes();
                    } 
                    else {
                        return array( 'success' => false,
                                      'message' => 'This Route Requires a GET Request..' );
                    }
                } else {
                    return array( 'success' => false,
                                  'message' => 'Unauthorized Access!' );
                }
                break;

            /**
             * /gct/ GET CAMPAIGN TYPES BY ID
             * @api {get} /campaigns/gcti/:id
             *
             *
             * @apiSuccess Returns a two-dimensional array of campaign_type records.
             */
            case "gcti":
                if ($this->authorized === true) {
                    if ($this->method == 'GET') {
                        return $campaign_da->getCampaignTypesById($this->args[0]);
                    } 
                    else {
                        return array( 'success' => false,
                                      'message' => 'This Route Requires a GET Request..' );
                    }
                } else {
                    return array( 'success' => false,
                                  'message' => 'Unauthorized Access!' );
                }
            break;
            
            /**
             * /gcbs/ GET CAMPAIGN BLAST STATS
             * @api {post} /campaigns/gcbs/:id
             *
             * -- POST BODY --
             * @apiParam {id} - id associated with the stats
             *
             * @apiSuccess Returns a two-dimensional array that contains the following fields:
             *               blasts_sent, emails_sent, emails_opened, links_opened
             */
            case 'gcbs':
                if ($this->authorized === true) {
                    if ($this->method == 'GET') {
                        return $campaign_da->getCampaignBlastStats($this->args[0]);
                    } 
                    else {
                        return array( 'success' => false,
                                      'message' => 'This Route Requires a GET Request..' );
                    }
                } else {
                    return array( 'success' => false,
                                  'message' => 'Unauthorized Access!' );
                }
                break;

            /**
             * /cceb/ GET ALL CAMPAIGN BLAST STATS
             * @api {post} /campaigns/gacbs/
             *
             * -- POST BODY --
             * @apiParam {id} - id associated with the stats
             *
             * @apiSuccess Returns a two-dimensional array that contains the following fields:
             *               email_blast_id, emails_sent, emails_opened, links_opened
             */
            case 'gacbs':
                if ($this->authorized === true) {
                    if ($this->method == 'GET') {
                        return $campaign_da->getAllCampaignBlastStats($this->args[0]);
                    } 
                    else {
                        return array( 'success' => false,
                                      'message' => 'This Route Requires a GET Request..' );
                    }
                } else {
                    return array( 'success' => false,
                                  'message' => 'Unauthorized Access!' );
                }
                break;

            /**
             * /uc/ UPDATE CAMPAIGN
             * @api {put} /campaigns/uc/
             *
             * -- PUT BODY --
             * @apiParam {campaign_id} - id of the campaign that will be updated
             * @apiParam {customer_id} - customer_id of the updated campaign
             * @apiParam {title} - title of the updated campaign
             * @apiParam {content} - content of the updated campaign
             * @apiParam {url} - URL of the updated campaign
             * @apiParam {types} - types of the updated campaign
             *
             * @apiSuccess Returns a boolean indicating whether the procedure was successful or not.
             */
            case "uc":
                if ($this->authorized === true) {
                    if ($this->method == 'PUT') {
                        return $campaign_da->updateCampaign($this->data['campaign_id'], $this->data['customer_id'], $this->data['title'], $this->data['content'], $this->data['url'], $this->data['types']);
                    } 
                    else {
                        return array( 'success' => false,
                                      'message' => 'This Route Requires a PUT Request..' );
                    }
                } else {
                    return array( 'success' => false,
                                  'message' => 'Unauthorized Access!' );
                }
                break;

            /**
             * /cceb/ CREATE CAMPAIGN EMAIL BLAST
             * @api {post} /campaigns/cceb/
             *
             * -- POST BODY --
             * @apiParam {id} - id associated with the campaign that the email blast is associated with
             *
             * @apiSuccess Returns a two-dimensional array that contains the following fields:
             *               subscriber_id, email_blast_id, email, hash
             */
            case "cceb":
                if ($this->authorized === true) {
                    if ($this->method == 'POST') {
                        $results =  $campaign_da->createCampaignEmailBlast($this->data['campaign_id']);

                        if($results) {
                            foreach ($results as $eachRow) {
                                $ID = $eachRow['subscriber_id'];
                                $HASH = $eachRow['hash'];
                                $EMAIL = $eachRow['email'];
                                $BLASTID = $eachRow['email_blast_id'];
                                
                                // SEND EMAIL
                                $result  = sendSingleEmail($ID, $HASH, $EMAIL, $BLASTID);

                                if ($result) {
                                    //echo "SUCCESS";
                                } else {
                                    return "failed send email";
                                }
                            }
                            return "true";
                        } else {
                            return "false";
                        }
                    } 
                    else {
                        return array( 'success' => false,
                                      'message' => 'This Route Requires a POST Request..' );
                    }
                } else {
                    return array( 'success' => false,
                                  'message' => 'Unauthorized Access!' );
                }
                break;

            default:
                return array( 'success' => false,
                              'message' => 'Unknown Route' );
                break;
        }     
     }
}

function sendSingleEmail($ID, $HASH, $EMAIL, $BLASTID)
{
    // Link is
    // subscriber ID+ hash + 
    // URL TO IMG SRC
    
    $imgURL  = "https://zahfox.com/acme/includes/email/TrackAPI.php?ID=$ID&HASH=$HASH&BID=$BLASTID&IMAGE=TRUE";
    $linkURL = "https://zahfox.com/acme/includes/email/TrackAPI.php?ID=$ID&HASH=$HASH&BID=$BLASTID";


    //GET BODY
    global $campaign_da;
    $BODY = $campaign_da->getCampaigns('blast', $BLASTID)[0]['content'];
    $TITLE = $campaign_da->getCampaigns('blast', $BLASTID)[0]['title'];

    // EMAIL HEADERS FROM
    $from_email = "admin@acme.zahfox.com";
    $from_name  = "ACME Emails";
    
    // EMAIL HEADERS TO
    $to_name  = "Email From Acme";
    $to_email = $EMAIL;

   
    
    // BODY
$body = '
<html>
    <body>
        <center><img align="middle" src="http://www.theshelbyreport.com/wp-content/uploads/2013/01/acmelogo.jpg"></center>
      <h1 align="center">' . $TITLE . '</h1>
      <h4 align="center">' . $BODY . '</h1>
      <center><a href="' . $linkURL . '">Click here to visit the site!</a></center>
      <img src="' . $imgURL . '" alt="no pic">
    </body>
</html>
';
    
    // To send HTML mail, the Content-type header must be set
    
    $headers = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
    $headers .= 'Cache-Control: no-cache, no-store, must-revalidate' . "\r\n";
    
    // Additional headers
    
    $headers .= 'To: ' . $to_name . ' <' . $to_email . '>' . "\r\n";
    $headers .= 'From: ' . $from_name . ' <' . $from_email . '>' . "\r\n";
    $result = mail($to_email, "TESTING EMAILS", $body, $headers);
    if ($result) {
        return true;
    } else {
        return false;
    }
}