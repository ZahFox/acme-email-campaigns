<?php
class UserAPI extends API
{
    public function __construct($request, $origin) {
        parent::__construct($request);
    }
    /*
    USERS ENDPOINTS

    /users

    */
    protected function users() {
        global $user_da;

        switch ($this->verb) {
            /**
             * /gu - GET USER
             * @api {get} /users/gu
             *
             * @apiSuccess Returns an associative array of User records.
             */
            case 'gu':
                if ($this->authorized === true) {
                    if ($this->method == 'GET') {
                        return $user_da->getUser(USER_ID);
                    } 
                    else {
                        return array( 'success' => false,
                                      'message' => 'This Route Requires a GET Request..' );
                    }
                } else {
                    return array( 'success' => false,
                                  'message' => 'Unauthorized Access!' );
                }
                break;


            /**
             * /gus - GET USERS
             * @api {get} /users/gus
             *
             * @apiSuccess Returns an associative array of User records.
             */
            case 'gus':
            if ($this->authorized === true && $this->admin === true) {
                if ($this->method == 'GET') {
                    return $user_da->getUsers();
                } 
                else {
                    return array( 'success' => false,
                                  'message' => 'This Route Requires a GET Request..' );
                }
            } else {
                return array( 'success' => false,
                              'message' => 'Unauthorized Access!' );
            }
            break;


            /**
             * /delu - DELETE USERS
             * @api {put} /users/delu
             *
             * @apiSuccess Returns a success message.
             */
            case 'delu':
            if ($this->authorized === true && $this->admin === true) {
                if ($this->method == 'PUT') {
                    $success = $user_da->deleteUser($this->data['email']);
                    if ($success === true) {
                        return array('success' => true,
                                     'message' => 'User Successfully Deleted!');
                    }  else {
                        return array('success' => false,
                                     'message' => 'User was not Deleted!');
                    }
                } 
                else {
                    return array( 'success' => false,
                                  'message' => 'This Route Requires a PUT Request..' );
                }
            } else {
                return array( 'success' => false,
                              'message' => 'Unauthorized Access!' );
            }
            break;


            /**
             * /gc - GET CUSTOMERS
             * @api {get} /users/gc
             *
             * @apiSuccess Returns a two-dimensional array that has the following properties: 
             *`customer_id` and `name`
             */
            case 'gc':
                if ($this->authorized === true) {
                    if ($this->method == 'GET') {
                        return $user_da->getCustomers();
                    } 
                    else {
                        return array( 'success' => false,
                                      'message' => 'This Route Requires a GET Request..' );
                    }
                } else {
                    return array( 'success' => false,
                                  'message' => 'Unauthorized Access!' );
                }
                break;

            
            /**
             * /uc - UPDATE CUSTOMERS
             * @api {put} /users/uc
             *
             * @apiSuccess A success message
             */
            case 'uc':
            if ($this->authorized === true && $this->admin === true) {
                if ($this->method == 'PUT') {
                    $success = $user_da->updateCustomer($this->data['customer_id'], $this->data['name']);
                    if ($success === true) {
                        return array('success' => true,
                                     'message' => 'Customer Information Successfully Updated!');
                    } else {
                        return array('success' => false,
                                     'message' => 'Customer Information was not Updated');
                    }
                } 
                else {
                    return array( 'success' => false,
                                  'message' => 'This Route Requires a PUT Request..' );
                }
            } else {
                return array( 'success' => false,
                              'message' => 'Unauthorized Access!' );
            }
            break;
            

            /**
             * /delc - DELETE CUSTOMERS
             * @api {delete} /users/delc
             *
             * @apiSuccess Returns a success message.
             */
            case 'delc':
            if ($this->authorized === true && $this->admin === true) {
                if ($this->method == 'DELETE') {
                    $success = $user_da->deleteCustomer($this->args[0]);
                    if ($success === true) {
                        return array('success' => true,
                                     'message' => 'Customer Successfully Deleted!');
                    } else {
                        return array('success' => false,
                                     'message' => 'User Information was not Updated');
                    }
                } else {
                    return array( 'success' => false,
                                  'message' => 'This Route Requires a DELETE Request..' );
                }
            } else {
                return array( 'success' => false,
                              'message' => 'Unauthorized Access!' );
            }
            break;


            /**
             * /login - LOGIN
             * @api {post} /users/login
             * 
             * @apiSuccess Returns a boolean that indicates whether or not the login was successful
             */
            case 'login':
                if ($this->method == 'POST') {
                    // Remove the previous session
                    $_SESSION = array();
                    session_destroy();
                    $email = $this->data['email'];
                    $password = $this->data['password'];
                    $user = $user_da->login($email, $password);

                    if ($user) {
                        session_set_cookie_params(time()+1800, '/', SITE_DOMAIN, SSL, true);
                        session_start();
                        $_SESSION['created'] = time();
                        $_SESSION['user_id'] = $user['user_id'];
                        $_SESSION['user_type'] = $user['user_type'];
                        return array('success' => true,
                                     'message' => 'Login Successful!');
                    }
                    return array('success' => false);
                } 
                else {
                    return array( 'success' => false,
                                  'message' => 'This Route Requires POST Request..' );
                }
                break;


            /**
             * /auth - AUTHORIZE
             * @api {post} /users/auth
             * 
             * @apiSuccess Returns a boolean that indicates whether or not a client is authorized
             */
            case 'auth':
            if ($this->authorized === true) {
                return array( 'success' => true,
                              'message' => 'Authorized Access!' );
            } else {
                return array( 'success' => false,
                              'message' => 'Unauthorized Access!' );
            }
            break;


            /**
             * /uul/ UPDATE USER LOGIN
             * @api {put} /users/uul/
             *
             * -- PUT BODY --
             * @apiParam {old_email} - the current user email
             * @apiParam {old_password} - the current user password
             * @apiParam {new_email} - the new user email
             * @apiParam {new_password} - the new user password
             *
             * @apiSuccess Returns a boolean indicating whether the procedure was successful or not.
             */
            case "update":
            if ($this->authorized === true) {
                if ($this->method == 'PUT') {
                    $success = $user_da->updateLogin(USER_ID, $this->data['old_email'], $this->data['old_password'], $this->data['new_email'], $this->data['new_password']);
                    if ($success === true) {
                        return array('success' => true,
                                     'message' => 'User Information Successfully Updated!');
                    } else {
                        return array('success' => false,
                                     'message' => 'User Information was not Updated');
                    }
                } 
                else {
                    return array( 'success' => false,
                                  'message' => 'This Route Requires a PUT Request..' );
                }
            } else {
                return array( 'success' => false,
                              'message' => 'Unauthorized Access!' );
            }
            break;


            /**
             * /cu CREATE USER
             * @api {post} /users/cu
             *
             * -- POST BODY --
             * @apiParam {email} - the new User email address
             * @apiParam {password} - the new User password
             *
             * @apiSuccess Returns a boolean indicating whether the procedure was successful or not.
             */
            case "cu":
            if ($this->authorized === true && $this->admin === true) {
                if ($this->method == 'POST') {
                    $success = $user_da->createUser($this->data['email'], $this->data['password']);
                    if ($success === true) {
                        return array('success' => true,
                                     'message' => 'New User Created!');
                    } else {
                        return array('success' => false,
                                     'message' => 'User was not Created!');
                    }
                } 
                else {
                    return array( 'success' => false,
                                  'message' => 'This Route Requires a POST Request..' );
                }
            } else {
                return array( 'success' => false,
                              'message' => 'Unauthorized Access!' );
            }
            break;


            /**
             * /cu CREATE CUSTOMER
             * @api {post} /users/cc
             *
             * -- POST BODY --
             * @apiParam {name} - the new Customer's name
             *
             * @apiSuccess Returns a boolean indicating whether the procedure was successful or not.
             */
            case "cc":
            if ($this->authorized === true && $this->admin === true) {
                if ($this->method == 'POST') {
                    $success = $user_da->createCustomer($this->data['name']);
                    if ($success === true) {
                        return array('success' => true,
                                     'message' => 'New Customer Created!');
                    } else {
                        return array('success' => false,
                                     'message' => 'Customer was not Created!');
                    }
                } 
                else {
                    return array( 'success' => false,
                                  'message' => 'This Route Requires a POST Request..' );
                }
            } else {
                return array( 'success' => false,
                              'message' => 'Unauthorized Access!' );
            }
            break;


            default:
                return array( 'success' => false,
                              'message' => 'Unknown Route' );
                break;
        }
    }
}