import axios from 'axios'

  export function createCampaign (data, callback) {
    axios({
      method: 'post',
      url: 'https://acme.zahfox.com/api/campaigns/cc',
      headers: {
          'Content-Type': 'application/json'
      },
      data: {
        customer_id: data.customer,
        title: data.title,
        content: data.content,
        url: data.url,
        types: data.types
      }
    }).then(res => callback(res))
  }

  export function updateCampaign (data, callback) {
    axios({
      method: 'put',
      url: 'https://acme.zahfox.com/api/campaigns/uc',
      headers: {
          'Content-Type': 'application/json'
      },
      data: {
        campaign_id: data.campaign_id,
        customer_id: data.customer_id,
        title: data.title,
        content: data.content,
        url: data.url,
        types: data.types
      }
    }).then(res => callback(res))
  }
  
  export function createEmailBlast (id, callback) {
    axios({
        method: 'post',
        url: 'https://acme.zahfox.com/api/campaigns/cceb',
        headers: {
            'Content-Type': 'application/json'
        },
        data: {
          campaign_id: id,
        }
      }).then(res => callback(res))
  } 

  export function getAllCampaigns (callback) {
    axios.get('https://acme.zahfox.com/api/campaigns/gc/all')
         .then((res => callback(res)))
  }

  export function getCampaignById (id, callback) {
    axios.get(`https://acme.zahfox.com/api/campaigns/gc/campaign/${id}`)
         .then((res => callback(res)))
  }

  export function getCampaignsByUser (callback, any) {
    axios.get(`https://acme.zahfox.com/api/campaigns/gc/user`)
         .then((res => callback(res)))
          .finally( () => {
            if (any) any()
          })
  }

  export function getAllCampaignBlastStats (id, callback) {
    axios.get(`https://acme.zahfox.com/api/campaigns/gacbs/${id}`)
         .then((res => callback(res)))
  }

  export function getCampaignTypesById (id, callback) {
    axios.get(`https://acme.zahfox.com/api/campaigns/gcti/${id}`)
    .then((res => callback(res)))
  }

  export function getAllCampaignTypes (callback) {
    axios.get('https://acme.zahfox.com/api/campaigns/gct')
    .then((res => callback(res)))
  }