import axios from 'axios'

export function getCampaignEmails (id, callback) {
    axios.get(`https://acme.zahfox.com/api/emails/geb/${id}`)
         .then((res => callback(res)))
}