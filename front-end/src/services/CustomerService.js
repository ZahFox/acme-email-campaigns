import axios from 'axios'

export function getCustomers (callback) {
  axios.get('https://acme.zahfox.com/api/users/gc')
  .then((res => callback(res)))
}

export function deleteCustomer (id, callback) {
  axios({
    method: 'delete',
    url: `https://acme.zahfox.com/api/users/delc/${id}`,
    headers: {
        'Content-Type': 'application/json'
    }
  }).then( res => callback(res))
}

export function createCustomer (name, callback) {
  axios({
    method: 'post',
    url: 'https://acme.zahfox.com/api/users/cc',
    headers: {
        'Content-Type': 'application/json'
    },
    data: {
      name: name
    }
  }).then(res => callback(res))
}