import axios from 'axios'

  export function getSubscribers (start, max, callback) {
    axios.get(`https://acme.zahfox.com/api/subscribers/gs/${start}/${max}`)
         .then(( res => callback(res)))
  }

  export function deleteSubscriber (id, callback) {
    axios({
      method: 'delete',
      url: `https://acme.zahfox.com/api/subscribers/ds/${id}`,
      headers: {
          'Content-Type': 'application/json'
      }
    }).then( res => callback(res))
  }