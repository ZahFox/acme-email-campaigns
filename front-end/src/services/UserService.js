import axios from 'axios'

  export function getUser (callback) {
    axios.get('https://acme.zahfox.com/api/users/gu')
         .then(( res => callback(res)))
  }

  export function getUsers (callback) {
    axios.get('https://acme.zahfox.com/api/users/gus')
         .then(( res => callback(res)))
  }

  export function createUser (email, password, callback) {
    axios({
      method: 'post',
      url: 'https://acme.zahfox.com/api/users/cu',
      headers: {
          'Content-Type': 'application/json'
      },
      data: {
        email: email,
        password: password 
      }
    }).then( res => callback(res))
  }

  export function updateUser (oldEmail, newEmail, oldPassword, newPassword, callback) {
    axios({
      method: 'put',
      url: 'https://acme.zahfox.com/api/users/update',
      headers: {
          'Content-Type': 'application/json'
      },
      data: {
        old_email: oldEmail,
        new_email: newEmail,
        old_password: oldPassword,
        new_password: newPassword
      }
    }).then( res => callback(res))
  }

  export function deleteUser (email, callback) {
    axios({
      method: 'put',
      url: 'https://acme.zahfox.com/api/users/delu',
      headers: {
          'Content-Type': 'application/json'
      },
      data: {
        email: email,
      }
    }).then( res => callback(res))
  }

  export function login (email, password, callback) {
    axios({
      method: 'post',
      url: 'https://acme.zahfox.com/api/login',
      headers: {
          'Content-Type': 'application/json'
      },
      data: {
        email: email,
        password: password,
      }
    }).then( res => callback(res))
  }

  export function authorize (callback) {
    axios({
      method: 'post',
      url: 'https://acme.zahfox.com/api/users/auth',
      headers: {
          'Content-Type': 'application/json'
      }
    }).then( res => callback(res))
  }
