import Vue from 'vue'
import Vuex from 'vuex'
import { getUser, authorize } from '../services/UserService'
import { getAllCampaignTypes } from '../services/CampaignService'
import { getCustomers } from '../services/CustomerService'
import { setTimeout } from 'timers';

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    apiURL: 'https://acme.zahfox.com/api/',
    user: {},
    campaignTypes: [],
    customers: [],
    authorized: false,
    lastRoute: null,
    startup: true
  },

  getters: {
    getCampaignById: state => id => {
      return state.campaigns.find(campaign => campaign.campaign_id == id)
    }
  },

  mutations: {
    setUser: (state, data) => {
      state.user = data[0]
    },

    setCampaignTypes: (state, data) => {
      state.campaignTypes = data
    },

    setCustomers: (state, data) => {
      state.customers = data
    },

    setAuthorized: (state, data) => {
      state.authorized = data
    },

    setLastRoute: (state, data) => {
      state.lastRoute = data
    },

    setStartupOver: (state) => {
      state.startup = false
    },

    disableAuth: (state) => {
      state.authorized = false
    }
  },

  actions: {
    endStartup ( {commit} ) {
      commit('setStartupOver')
    },

    endAuth ( {commit} ) {
      commit('disableAuth')
    },

    async loadUser ( {commit} ) {
      await getUser( res => {     
        commit('setUser', res.data)
      })
    },

    async loadCampaignTypes ( {commit} ) {
      await getAllCampaignTypes( res => {
        commit('setCampaignTypes', res.data)
      })
    },

    async loadCustomers ( {commit} ) {
      await getCustomers( res => {
        commit('setCustomers', res.data)
      })
    },

    async checkAuth ( {commit} ) {
      await authorize( res => {
        commit('setAuthorized', res.data.success)
      })
    },

    checkAuthSync ( req, payload ) {
      authorize( res => {
        req.commit('setAuthorized', res.data.success)
        payload.callback()
      })
    },

    async storeLastRoute ( {commit}, value ) {
      await commit('setLastRoute', value)
    }
  }

})