import Vue from 'vue'
import Router from 'vue-router'
import store from '../store'
import Campaigns from '@/components/Campaigns'
import NewCampaign from '@/components/NewCampaign'
import EditCampaign from '@/components/EditCampaign'
import CampaignDetails from '@/components/CampaignDetails'
import EmailBlastDetails from '@/components/EmailBlastDetails'
import AccountSettings from '@/components/AccountSettings'
import StatisticsHome from '@/components/StatisticsHome'
import ControlPanel from '@/components/ControlPanel'
import Login from '@/components/Login'
import Loading from '@/components/Loading'

Vue.use(Router)

const router = new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Campaigns
    },
    {
      path: '/campaigns',
      name: 'Campaigns',
      component: Campaigns
    },
    {
      path: '/campaigns/new',
      name: 'NewCampaign',
      component: NewCampaign
    },
    {
      path: '/campaigns/:id/details',
      name: 'CampaignDetails',
      component: CampaignDetails,
      props: true
    },
    {
      path: '/campaigns/edit/:id',
      name: 'EditCampaign',
      component: EditCampaign,
      props: true
    },
    {
      path: '/campaigns/:campaign-id/email/:email-id',
      name: 'EmailBlast',
      component: EmailBlastDetails
    },
    {
      path: '/account',
      name: 'AccountSettings',
      component: AccountSettings
    },
    {
      path: '/statistics',
      name: 'Statistics',
      component: StatisticsHome
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/control-panel',
      name: 'ControlPanel',
      component: ControlPanel
    },
    {
      path: '/loading',
      name: 'Loading',
      component: Loading
    }
  ]
})

router.beforeEach((to, from, next) => {
  
  if (store.state.startup == false) {

    store.dispatch('storeLastRoute', from.name)

    if (store.state.authorized === false) {
      if (to.name !== 'Login') {
        next('/login')
      }
      else {
        next()
      }
    } else if (store.state.authorized === true) {
      if (to.name == 'Login' || to.name == 'Loading') {
        next('/campaigns')
      }
      else {
        next()
      }
    }
  }
  else {
    if (to.name != 'Loading') {
      next('/loading')
    } else {
      next()
    }
  }


  })

export default router