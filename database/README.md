ACME Email Campaigns :: Set Up and Test the Database
====================================================

#### >> Current Version: v0.5.0


## Prerequisites

![alt text](http://www.w3school.in/wp-content/uploads/2016/11/mysql_hosting.png "MySQL")

* The database for this project was created using [*MySQL Community Server version 5.6.37*](https://dev.mysql.com/doc/refman/5.6/en/). If you are using a diffrent version of MySQL or MariaDB, it can't be guarenteed that the SQL scripts will work for you.

***

## STEP 1: Create the Database

>Whether you are using [phpMyAdmin](https://www.phpmyadmin.net/), a command-line client, or accessing the MySQL database server another way, create a new database. After you create the database you may use the [db_baseline](./db_baseline_v0.3.0.sql) script to fill your new database with all of necessary tables and stored procedures for this project. This SQL script will also create all of the necessary constraints and indexes for each of the tables.

***

## STEP 2: (OPTIONAL) Sample Data and Testing

>As of version 0.4.0, if you would like to test the database please follow this step. After you have created a database and executed the [db_baseline](./db_baseline_v0.3.0.sql) script, you may use the [sample_inserts](./sample_inserts_v0.3.0.sql) script to fill the Customer and Campaign_Type tables with sample data. This script will also create a new stored procedure called: bootstrap_database. To finish adding sample data, execute this new stored procedure by using the following SQL query: `CALL bootstrap_database();`. After bootstrap_database has been executed, it will no longer be needed and may be deleted using the following SQL query: `DROP PROCEDURE IF EXISTS bootstrap_database;`. Once the database has been filled with sample data, queries from the [testing_queries](./testing_queries_v0.3.0.sql) script and the stored procedures may be used to test the database. Feel free to create your own queries or stored procedures to test this data, or to create more sample data. Referencing the [Database ERD](./ACME_EMAIL_CAMPAIGNS_ERD.png) will help you do this.