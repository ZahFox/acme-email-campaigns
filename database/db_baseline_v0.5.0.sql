SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `bootstrap_db` ()  BEGIN

    CALL `create_campaign_type`('Technology');
    CALL `create_campaign_type`('Sports');
    CALL `create_campaign_type`('Science');
    CALL `create_customer`('Aperture Laboratories');
    CALL `create_customer`('Curiosity Shop');
    CALL `create_customer`('Gold Saucer');
    CALL `create_user`('admin@acme.com', '$2y$10$XwoobLoNpKHnquivlDWMyeNaTSEWAAN7RcWTR6S.dGE.l1LsOOXHi');
    CALL `create_user`('employee01@acme.com', '$2y$10$D1XzFa6G97G7lXGYkBgnIe4zyn.IYScdoOuw0UKCmyr9HA/4fLdMu');
    CALL `create_user`('employee02@acme.com', '$2y$10$/kWS6DsKWM.5.G9MYRgWSuQvO7kN6TSS2AU4BijTUDEhXs26Oqusa');
    CALL `create_campaign`('1', '2', 'Gravity Gun 2.0', 'Hello World!', 'http://http://half-life.wikia.com/wiki/Zero_Point_Energy_Field_Manipulator', '1,3,#');
    CALL `create_campaign`('2', '2', 'Big Bomb Bag Super Sale', 'Hello World!', 'https://zelda.gamepedia.com/Bomb_Bag', '2,#');
    CALL `create_campaign`('3', '3', 'Chocobo Racing 2017 World Series', 'Hello World!', 'http://www.finalfantasy.wikia.com/wiki/Chocobo_Square_(Final_Fantasy_VII)', '1,2,3,#');
    CALL `create_subscriber`('sample01@test.com', '1,2,#');
    CALL `create_subscriber`('sample02@test.com', '2,3,#');
    CALL `create_subscriber`('sample03@test.com', '3,#');
    CALL `create_subscriber`('sample04@test.com', '1,#');
    CALL `create_subscriber`('sample05@test.com', '1,2,3,#');
    CALL `create_campaign_email_blast`(1);
    CALL `create_campaign_email_blast`(2);
    CALL `create_campaign_email_blast`(3);

  END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `check_campaign_lock` (IN `i_id` INT UNSIGNED)  BEGIN

    SELECT locked FROM Campaign WHERE campaign_id = i_id;

  END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `check_email_opened` (IN `i_id` INT UNSIGNED)  BEGIN

    SELECT email_opened FROM Email WHERE email_id = i_id;

  END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `check_link_opened` (IN `i_id` INT UNSIGNED)  BEGIN

    SELECT link_opened FROM Email WHERE email_id = i_id;

  END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `check_user_login` (IN `i_email` VARCHAR(50))  BEGIN

    SELECT * FROM User u WHERE u.email = i_email;

  END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `create_campaign` (IN `i_customer_id` SMALLINT UNSIGNED, IN `i_user_id` SMALLINT UNSIGNED, IN `i_title` VARCHAR(50), IN `i_content` VARCHAR(10000), IN `i_url` VARCHAR(100), IN `i_types` VARCHAR(100))  MODIFIES SQL DATA
BEGIN
    DECLARE success BOOLEAN;
    DECLARE types_created SMALLINT;
    DECLARE rows_created SMALLINT;
    DECLARE n_campaign_id SMALLINT UNSIGNED;
    DECLARE time_created TIMESTAMP;
    DECLARE type_id SMALLINT;
    SET types_created = 0;
    SET autocommit = 0;
    SET success = 0;

    START TRANSACTION;
      SET time_created = NOW();
      INSERT INTO Campaign (`campaign_id`, `customer_id`, `user_id`, `title`, `content`, `url`, `created`) VALUES
      (NULL, i_customer_id, i_user_id, i_title, i_content, i_url, time_created);
      SET rows_created = ROW_COUNT();

      SET n_campaign_id = (SELECT c.campaign_id 
        FROM Campaign c
        WHERE c.created = time_created AND c.user_id = i_user_id AND c.title = i_title);

      WHILE (LOCATE('#', i_types) > 1)
      DO
        SET type_id = SUBSTRING( i_types, 1, (LOCATE(',', i_types) - 1) );
        SET i_types = SUBSTRING( i_types, (LOCATE(',', i_types) + 1) );
        INSERT INTO Campaign_Type_Campaign (`campaign_id`, `campaign_type_id`) VALUES(n_campaign_id, type_id);
        SET types_created = ROW_COUNT() + types_created;
      END WHILE;

      IF (types_created < 1 OR rows_created <> 1) THEN
        ROLLBACK;
      ELSE
        SET success = 1;
      END IF;
    COMMIT;

    SELECT success;

  END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `create_campaign_email_blast` (IN `i_campaign_id` INT UNSIGNED)  BEGIN
    DECLARE e_success BOOLEAN;
    DECLARE e_blast_id INT;
    DECLARE fin INT DEFAULT 0;
    DECLARE s_id INT;
    DECLARE s_email VARCHAR(50);
    DECLARE time_created TIMESTAMP;

    DECLARE email_cursor CURSOR FOR
      SELECT DISTINCT *
        FROM Subscriber s
        WHERE s.subscriber_id IN
        (
          SELECT cts.subscriber_id
            FROM Campaign_Type_Subscriber cts
            WHERE cts.campaign_type_id IN
            (
              SELECT ct.campaign_type_id
                FROM Campaign_Type ct
                WHERE ct.campaign_type_id IN
                (
                  SELECT ctc.campaign_type_id
                    FROM Campaign_Type_Campaign ctc
                    JOIN Campaign c ON c.campaign_id = ctc.campaign_id
                    WHERE c.campaign_id = i_campaign_id
                )
            )
        );

      DECLARE CONTINUE HANDLER FOR NOT FOUND SET fin = 1;
      SET e_success = 0;
      SET autocommit = 0;
      SET time_created = NOW();
      START TRANSACTION;

        INSERT INTO Email_Blast (`email_blast_id`, `campaign_id`, `time_sent`) VALUES (NULL, i_campaign_id, time_created);
        SET e_blast_id = (SELECT eb.email_blast_id FROM Email_Blast eb WHERE eb.time_sent = time_created
          AND eb.campaign_id = i_campaign_id);
        CALL lock_campaign(i_campaign_id);
        
        OPEN email_cursor;
          get_email: LOOP

            FETCH email_cursor INTO s_id, s_email;

            IF fin = 1 THEN
              LEAVE get_email;
            END IF;

            CALL create_email(i_campaign_id, e_blast_id, s_id, time_created, e_success);
            IF e_success = 0 THEN
              ROLLBACK;
            END IF;

          END LOOP get_email;
        CLOSE email_cursor;
      COMMIT;

      IF e_success = 1 THEN
        SELECT s.subscriber_id, e.email_blast_id, s.email, e.hash
          FROM Email e
          JOIN Subscriber s ON s.subscriber_id = e.subscriber_id
          WHERE s.subscriber_id IN
          (
            SELECT DISTINCT s2.subscriber_id
                FROM Subscriber s2
                WHERE s2.subscriber_id IN
                (
                  SELECT cts.subscriber_id
                    FROM Campaign_Type_Subscriber cts
                    WHERE cts.campaign_type_id IN
                    (
                      SELECT ct.campaign_type_id
                        FROM Campaign_Type ct
                        WHERE ct.campaign_type_id IN
                        (
                          SELECT ctc.campaign_type_id
                            FROM Campaign_Type_Campaign ctc
                            JOIN Campaign c ON c.campaign_id = ctc.campaign_id
                            WHERE c.campaign_id = i_campaign_id
                        )
                    )
                )
          ) AND e.email_blast_id = e_blast_id;
      END IF;
      
  END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `create_campaign_type` (IN `i_name` VARCHAR(50))  BEGIN
    DECLARE success BOOLEAN;
    DECLARE rows_created SMALLINT;
    SET rows_created = 0;
    SET success = 0;

    START TRANSACTION;
      INSERT INTO `Campaign_Type` (`name`) VALUES (i_name);
      SET rows_created = ROW_COUNT();

      IF (rows_created <> 1) THEN
        ROLLBACK;
      ELSE
        SET success = 1;
      END IF;
    COMMIT;

    SELECT success;

  END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `create_customer` (IN `i_name` VARCHAR(50))  BEGIN
      DECLARE success BOOLEAN;
      DECLARE rows_created SMALLINT;
      SET rows_created = 0;
      SET success = 0;

      START TRANSACTION;
      INSERT INTO `Customer` (`name`) VALUES (i_name);
      SET rows_created = ROW_COUNT();

      IF (rows_created <> 1) THEN
        ROLLBACK;
      ELSE
        SET success = 1;
      END IF;
    COMMIT;

    SELECT success;

  END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `create_email` (IN `i_campaign_id` INT UNSIGNED, IN `i_email_blast_id` INT UNSIGNED, IN `i_subscriber_id` INT UNSIGNED, IN `i_time_sent` TIMESTAMP, OUT `success` BOOLEAN)  BEGIN
    DECLARE rows_updated SMALLINT;
    DECLARE random INT;
    DECLARE salt CHAR(32);
    DECLARE new_hash CHAR(42);
    SET rows_updated = 0;
    SET autocommit = 0;
    SET success = 0;
    SET random = FLOOR( 1 + (RAND() * 343 ) );
    SET salt = CAST(random AS CHAR(32));
    SET new_hash = SHA1(CONCAT(salt, i_campaign_id, i_subscriber_id, i_time_sent, salt));
    
    START TRANSACTION;
      INSERT INTO Email (`campaign_id`, `subscriber_id`, `hash`, `email_blast_id`) VALUES
        (i_campaign_id, i_subscriber_id, new_hash, i_email_blast_id);
      SET rows_updated = ROW_COUNT();
      IF rows_updated > 0 THEN
        SET success = 1;
      ELSE
        ROLLBACK;
      END IF;
    COMMIT;

  END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `create_subscriber` (IN `i_email` VARCHAR(50), IN `i_types` VARCHAR(100))  MODIFIES SQL DATA
BEGIN
    DECLARE success BOOLEAN;
    DECLARE types_created SMALLINT;
    DECLARE rows_created SMALLINT;
    DECLARE s_id SMALLINT;
    DECLARE type_id SMALLINT;
    SET types_created = 0;
    SET autocommit = 0;
    SET success = 0;

    START TRANSACTION;
      INSERT INTO Subscriber (`subscriber_id`, `email`) VALUES (NULL, i_email);
      SET rows_created = ROW_COUNT();

      SET s_id = (SELECT s.subscriber_id 
        FROM Subscriber s
        WHERE s.email = i_email);

      WHILE (LOCATE('#', i_types) > 1)
      DO
        SET type_id = SUBSTRING( i_types, 1, (LOCATE(',', i_types) - 1) );
        SET i_types = SUBSTRING( i_types, (LOCATE(',', i_types) + 1) );
        INSERT INTO Campaign_Type_Subscriber (`subscriber_id`, `campaign_type_id`) VALUES(s_id, type_id);
        SET types_created = ROW_COUNT() + types_created;
      END WHILE;

      IF (types_created < 1 OR rows_created <> 1) THEN
        ROLLBACK;
      ELSE
        SET success = 1;
      END IF;
    COMMIT;

    SELECT success;

  END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `create_user` (IN `i_email` VARCHAR(50), IN `i_password` CHAR(60))  BEGIN
    DECLARE success BOOLEAN;
    DECLARE rows_created SMALLINT;

    SET rows_created = 0;
    SET autocommit = 0;
    SET success = 0;

    START TRANSACTION;
      INSERT INTO User (`user_type`, `email`, `password`) VALUES('Employee', i_email, i_password);
      SET rows_created = ROW_COUNT();

      IF (rows_created <> 1) THEN
        ROLLBACK;
      ELSE
        SET success = 1;
      END IF;
    COMMIT;

    SELECT success;

  END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_campaign` (IN `i_id` INT UNSIGNED)  BEGIN
    DECLARE success BOOLEAN;
    DECLARE rows_deleted SMALLINT;
    SET rows_deleted = 0;
    SET autocommit = 0;
    SET success = 0;
    
    START TRANSACTION;
      DELETE FROM Campaign WHERE campaign_id = i_id;
      SET rows_deleted = ROW_COUNT();
      IF rows_deleted > 0 THEN
        SET success = 1;
      ELSE
        ROLLBACK;
      END IF;
    COMMIT;
    
    SELECT success;

  END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_customer` (IN `i_id` INT UNSIGNED)  BEGIN
    DECLARE success BOOLEAN;
    DECLARE rows_deleted SMALLINT;
    SET rows_deleted = 0;
    SET autocommit = 0;
    SET success = 0;
    
    START TRANSACTION;
      DELETE FROM Customer WHERE customer_id = i_id;
      SET rows_deleted = ROW_COUNT();
      IF rows_deleted > 0 THEN
        SET success = 1;
      ELSE
        ROLLBACK;
      END IF;
    COMMIT;

    SELECT success;

  END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_subscriber` (IN `i_id` INT UNSIGNED)  BEGIN
    DECLARE success BOOLEAN;
    DECLARE rows_deleted SMALLINT;
    SET rows_deleted = 0;
    SET autocommit = 0;
    SET success = 0;
    
    START TRANSACTION;
      DELETE FROM Subscriber WHERE subscriber_id = i_id;
      SET rows_deleted = ROW_COUNT();
      IF rows_deleted > 0 THEN
        SET success = 1;
      ELSE
        ROLLBACK;
      END IF;
    COMMIT;

    SELECT success;

  END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_user` (IN `i_email` VARCHAR(100))  BEGIN
    DECLARE success BOOLEAN;
    DECLARE rows_deleted SMALLINT;
    SET rows_deleted = 0;
    SET autocommit = 0;
    SET success = 0;
    
    START TRANSACTION;
      DELETE FROM User WHERE email = i_email;
      SET rows_deleted = ROW_COUNT();
      IF rows_deleted > 0 THEN
        SET success = 1;
      ELSE
        ROLLBACK;
      END IF;
    COMMIT;

    SELECT success;

  END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `fetch_all_campaign_blast_stats` (IN `i_id` INT UNSIGNED)  BEGIN
  SELECT eb.email_blast_id, 
    (SELECT COUNT(*) FROM Email e WHERE campaign_id = i_id AND
      email_blast_id = eb.email_blast_id) AS 'emails_sent',
    (SELECT COUNT(*) FROM Email e2 WHERE campaign_id = i_id AND email_opened = 1 AND
      email_blast_id = eb.email_blast_id) AS 'emails_opened',
    (SELECT COUNT(*) FROM Email e3 WHERE campaign_id = i_id AND link_opened = 1 AND
      email_blast_id = eb.email_blast_id) AS 'links_opened'
    FROM Email_Blast eb
    WHERE eb.campaign_id = i_id
    GROUP BY email_blast_id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `fetch_campaign` (IN `i_type` VARCHAR(10), IN `i_id` INT UNSIGNED)  BEGIN

    IF i_type = "campaign" THEN
      SELECT *
        FROM Campaign c
        WHERE c.campaign_id = i_id;
    ELSEIF i_type = "customer" THEN
      SELECT *
        FROM Campaign c
        WHERE c.customer_id = i_id;
    ELSEIF i_type = "user" THEN
      SELECT *
        FROM Campaign c
        WHERE c.user_id = i_id;
    ELSEIF i_type = "blast" THEN
      SELECT *
        FROM Campaign c
        WHERE c.campaign_id IN (
          SELECT eb.campaign_id
            FROM Email_Blast eb
            WHERE eb.email_blast_id = i_id
        );
    ELSEIF i_type = "all" THEN
      SELECT * FROM Campaign;
    END IF;

  END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `fetch_campaign_blast_stats` (IN `i_id` INT UNSIGNED)  BEGIN
  SELECT (SELECT COUNT(*) FROM Email_Blast WHERE campaign_id = i_id) AS 'blasts_sent', 
        (SELECT COUNT(*) FROM Email WHERE campaign_id = i_id) AS 'emails_sent',
        (SELECT COUNT(*) FROM Email WHERE campaign_id = i_id AND email_opened = 1) AS 'emails_opened',
        (SELECT COUNT(*) FROM Email WHERE campaign_id = i_id AND link_opened = 1) AS 'links_opened';
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `fetch_campaign_subscribers` (IN `i_campaign_id` INT UNSIGNED)  BEGIN

    SELECT DISTINCT *
      FROM Subscriber s
      WHERE s.subscriber_id IN
      (
        SELECT cts.subscriber_id
          FROM Campaign_Type_Subscriber cts
          WHERE cts.campaign_type_id IN
          (
            SELECT ct.campaign_type_id
              FROM Campaign_Type ct
              WHERE ct.campaign_type_id IN
              (
                SELECT ctc.campaign_type_id
                  FROM Campaign_Type_Campaign ctc
                  JOIN Campaign c ON c.campaign_id = ctc.campaign_id
                  WHERE c.campaign_id = i_campaign_id
              )
          )
      );

  END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `fetch_campaign_types` ()  BEGIN

    SELECT * FROM Campaign_Type;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `fetch_campaign_types_by_id` (IN `i_id` INT UNSIGNED)  BEGIN

    SELECT * FROM Campaign_Type ct
    WHERE ct.campaign_type_id IN
    (
      SELECT ctc.campaign_type_id 
        FROM Campaign_Type_Campaign ctc
        WHERE ctc.campaign_id = i_id
    );

  END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `fetch_customers` ()  BEGIN

    SELECT * FROM Customer;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `fetch_email_blast_emails` (IN `i_id` INT UNSIGNED)  BEGIN
    SELECT e.email_id, e.campaign_id, e.subscriber_id, e.email_opened,
      e.link_opened, e.email_blast_id
      FROM Email e
      WHERE e.email_blast_id = i_id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `fetch_email_blast_stats` (IN `i_id` INT UNSIGNED)  BEGIN
    SELECT (SELECT COUNT(*) FROM Email WHERE email_blast_id = i_id) AS 'emails_sent',
           (SELECT COUNT(*) FROM Email WHERE email_blast_id = i_id AND email_opened = 1) AS 'emails_opened',
           (SELECT COUNT(*) FROM Email WHERE email_blast_id = i_id AND email_opened = 1) AS 'links_opened';
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `fetch_email_campaign_blasts` (IN `i_id` INT UNSIGNED)  BEGIN

    SELECT *
      FROM Email_Blast eb
      WHERE eb.campaign_id = i_id
      ORDER BY eb.time_sent DESC;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `fetch_subscribers` (IN `i_start` INT UNSIGNED, IN `i_max` INT UNSIGNED)  BEGIN

    SELECT * FROM Subscriber ORDER BY subscriber_id LIMIT i_start,i_max;

  END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `fetch_user` (IN `i_id` INT UNSIGNED)  BEGIN
    SELECT u.user_type, u.email
      FROM User u
      WHERE u.user_id = i_id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `fetch_users` ()  BEGIN

    SELECT u.user_type, u.email
      FROM User u;

  END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `lock_campaign` (IN `i_id` INT UNSIGNED)  BEGIN
    DECLARE success BOOLEAN;
    DECLARE rows_updated SMALLINT;
    SET rows_updated = 0;
    SET autocommit = 0;
    SET success = 0;
    
    START TRANSACTION;
      UPDATE Campaign SET locked = 1 WHERE campaign_id = i_id;
      SET rows_updated = ROW_COUNT();
      IF rows_updated > 0 THEN
        SET success = 1;
      ELSE
        ROLLBACK;
      END IF;
    COMMIT;

  END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `open_email` (IN `i_subscriber_id` INT UNSIGNED, IN `i_hash` CHAR(42))  BEGIN
    DECLARE success BOOLEAN;
    DECLARE rows_updated SMALLINT UNSIGNED;
    SET autocommit = 0;
    SET success = 0;
    SET rows_updated = 0;

    START TRANSACTION;
      UPDATE Email SET `email_opened` = 1 WHERE subscriber_id = i_subscriber_id AND
          hash = i_hash;
      SET rows_updated = ROW_COUNT();
      IF rows_updated = 1 THEN
        SET success = 1;
      ELSE
        ROLLBACK;
      END IF;
    COMMIT;

    SELECT success;

  END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `open_link` (IN `i_subscriber_id` INT UNSIGNED, IN `i_hash` CHAR(42))  BEGIN
    DECLARE success BOOLEAN;
    DECLARE rows_updated SMALLINT UNSIGNED;
    SET autocommit = 0;
    SET success = 0;
    SET rows_updated = 0;

    START TRANSACTION;
      UPDATE Email SET `link_opened` = 1 WHERE subscriber_id = i_subscriber_id AND
          hash = i_hash;
      SET rows_updated = ROW_COUNT();
      IF rows_updated = 1 THEN
        SET success = 1;
      ELSE
        ROLLBACK;
      END IF;
    COMMIT;

    SELECT success;

  END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `update_campaign` (IN `i_id` INT UNSIGNED, IN `i_customer_id` SMALLINT UNSIGNED, IN `i_title` VARCHAR(50), IN `i_content` VARCHAR(10000), IN `i_url` VARCHAR(100), IN `i_types` VARCHAR(100))  BEGIN
    DECLARE success BOOLEAN;
    DECLARE is_locked BOOLEAN;
    DECLARE types_created SMALLINT;
    DECLARE rows_updated SMALLINT;
    DECLARE type_id SMALLINT;
    SET rows_updated = 0;
    SET autocommit = 0;
    SET success = 0;

    SET is_locked = (SELECT locked FROM Campaign WHERE campaign_id = i_id);
    IF is_locked <> TRUE THEN 
      START TRANSACTION;
        IF i_title <> "" THEN
          UPDATE Campaign SET title = i_title WHERE campaign_id = i_id;
          SET rows_updated = ROW_COUNT() + rows_updated;
        END IF;

        IF i_content <> "" THEN
          UPDATE Campaign SET content = i_content WHERE campaign_id = i_id;
          SET rows_updated = ROW_COUNT() + rows_updated;
        END IF;

        IF i_url <> "" THEN
          UPDATE Campaign SET url = i_url WHERE campaign_id = i_id;
          SET rows_updated = ROW_COUNT() + rows_updated;
        END IF;

        IF i_customer_id <> "" THEN
          UPDATE Campaign SET customer_id = i_customer_id WHERE campaign_id = i_id;
          SET rows_updated = ROW_COUNT() + rows_updated;
        END IF;

        IF i_types <> "" THEN
          DELETE FROM Campaign_Type_Campaign WHERE campaign_id = i_id;
          WHILE (LOCATE('#', i_types) > 1)
          DO
            SET type_id = SUBSTRING( i_types, 1, (LOCATE(',', i_types) - 1) );
            SET i_types = SUBSTRING( i_types, (LOCATE(',', i_types) + 1) );
            INSERT INTO Campaign_Type_Campaign (`campaign_id`, `campaign_type_id`) VALUES(i_id, type_id);
            SET types_created = ROW_COUNT() + types_created;
          END WHILE;

          SET rows_updated = ROW_COUNT() + rows_updated;
        END IF;

        IF rows_updated > 0 THEN
          SET success = 1;
        ELSE
          ROLLBACK;
        END IF;
      COMMIT;
    END IF;

    SELECT success;

  END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `update_customer` (IN `i_id` INT UNSIGNED, IN `i_name` VARCHAR(50))  BEGIN
    DECLARE success BOOLEAN;
    DECLARE rows_updated SMALLINT;
    SET rows_updated = 0;
    SET autocommit = 0;
    SET success = 0;
    
    START TRANSACTION;
      UPDATE Customer SET `name` = i_name WHERE `customer_id` = i_id;
      SET rows_updated = ROW_COUNT();
      IF (rows_updated <> 1) THEN
        ROLLBACK;
      ELSE
        SET success = 1;
      END IF;
    COMMIT;

    SELECT success;

  END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `update_user_login` (IN `i_id` SMALLINT UNSIGNED, IN `i_old_email` VARCHAR(50), IN `i_new_email` VARCHAR(50), IN `i_new_password` VARCHAR(100))  BEGIN
    DECLARE success BOOLEAN;
    DECLARE valid BOOLEAN;
    DECLARE rows_updated SMALLINT;
    SET rows_updated = 0;
    SET autocommit = 0;
    SET success = 0;
    SET valid = 0;

    SET valid = (SELECT COUNT(*)
                  FROM User u
                  WHERE u.email = i_old_email);

    IF valid = 1 THEN
      START TRANSACTION;
        UPDATE User u SET u.email = i_new_email, u.password = i_new_password
          WHERE u.user_id = i_id;
        SET rows_updated = ROW_COUNT();

        IF (rows_updated <> 1) THEN
          ROLLBACK;
        ELSE
          SET success = 1;
        END IF;
      COMMIT;
    END IF;
    
    SELECT success;

  END$$

DELIMITER ;

CREATE TABLE `Campaign` (
  `campaign_id` int(10) UNSIGNED NOT NULL,
  `customer_id` smallint(5) UNSIGNED DEFAULT NULL,
  `user_id` smallint(5) UNSIGNED DEFAULT NULL,
  `title` varchar(50) NOT NULL,
  `content` varchar(10000) NOT NULL,
  `url` varchar(100) NOT NULL,
  `locked` tinyint(1) NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `Campaign_Type` (
  `campaign_type_id` smallint(5) UNSIGNED NOT NULL,
  `name` enum('Technology','Sports','Science') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `Campaign_Type_Campaign` (
  `campaign_id` int(11) UNSIGNED NOT NULL,
  `campaign_type_id` smallint(6) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `Campaign_Type_Subscriber` (
  `subscriber_id` int(10) UNSIGNED NOT NULL,
  `campaign_type_id` smallint(5) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `Customer` (
  `customer_id` smallint(5) UNSIGNED NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `Email` (
  `email_id` int(10) UNSIGNED NOT NULL,
  `campaign_id` int(10) UNSIGNED NOT NULL,
  `subscriber_id` int(10) UNSIGNED DEFAULT NULL,
  `email_opened` tinyint(1) NOT NULL DEFAULT '0',
  `link_opened` tinyint(1) NOT NULL DEFAULT '0',
  `hash` char(42) NOT NULL,
  `email_blast_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `Email_Blast` (
  `email_blast_id` int(10) UNSIGNED NOT NULL,
  `campaign_id` int(10) UNSIGNED NOT NULL,
  `time_sent` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `Subscriber` (
  `subscriber_id` int(10) UNSIGNED NOT NULL,
  `email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `User` (
  `user_id` smallint(6) UNSIGNED NOT NULL,
  `user_type` enum('Admin','Employee') NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` char(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE `Campaign`
  ADD PRIMARY KEY (`campaign_id`),
  ADD KEY `customer_id` (`customer_id`),
  ADD KEY `user_id` (`user_id`);

ALTER TABLE `Campaign_Type`
  ADD PRIMARY KEY (`campaign_type_id`),
  ADD UNIQUE KEY `unique_campaign_type` (`name`);

ALTER TABLE `Campaign_Type_Campaign`
  ADD PRIMARY KEY (`campaign_id`,`campaign_type_id`),
  ADD KEY `campaign_type_id` (`campaign_type_id`);

ALTER TABLE `Campaign_Type_Subscriber`
  ADD PRIMARY KEY (`subscriber_id`,`campaign_type_id`),
  ADD KEY `subscriber_group_id` (`campaign_type_id`);

ALTER TABLE `Customer`
  ADD PRIMARY KEY (`customer_id`),
  ADD UNIQUE KEY `unique_name` (`name`) USING BTREE;

ALTER TABLE `Email`
  ADD PRIMARY KEY (`email_id`),
  ADD KEY `campaign_id` (`campaign_id`),
  ADD KEY `subscriber_id` (`subscriber_id`),
  ADD KEY `email_blast_id` (`email_blast_id`);

ALTER TABLE `Email_Blast`
  ADD PRIMARY KEY (`email_blast_id`),
  ADD KEY `campaign_id` (`campaign_id`);

ALTER TABLE `Subscriber`
  ADD PRIMARY KEY (`subscriber_id`),
  ADD UNIQUE KEY `unique_subscriber_email` (`email`) USING BTREE;

ALTER TABLE `User`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `unique_email` (`email`) USING BTREE;


ALTER TABLE `Campaign`
  MODIFY `campaign_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
ALTER TABLE `Campaign_Type`
  MODIFY `campaign_type_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT;
ALTER TABLE `Customer`
  MODIFY `customer_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT;
ALTER TABLE `Email`
  MODIFY `email_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
ALTER TABLE `Email_Blast`
  MODIFY `email_blast_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
ALTER TABLE `Subscriber`
  MODIFY `subscriber_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
ALTER TABLE `User`
  MODIFY `user_id` smallint(6) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `Campaign`
  ADD CONSTRAINT `FKc_customer_id` FOREIGN KEY (`customer_id`) REFERENCES `Customer` (`customer_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `FKc_user_id` FOREIGN KEY (`user_id`) REFERENCES `User` (`user_id`) ON DELETE SET NULL ON UPDATE CASCADE;

ALTER TABLE `Campaign_Type_Campaign`
  ADD CONSTRAINT `FKctc_campaign_id` FOREIGN KEY (`campaign_id`) REFERENCES `Campaign` (`campaign_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FKctc_campaign_type_id` FOREIGN KEY (`campaign_type_id`) REFERENCES `Campaign_Type` (`campaign_type_id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `Campaign_Type_Subscriber`
  ADD CONSTRAINT `FKcts_campaign_type` FOREIGN KEY (`campaign_type_id`) REFERENCES `Campaign_Type` (`campaign_type_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FKcts_subscriber_id` FOREIGN KEY (`subscriber_id`) REFERENCES `Subscriber` (`subscriber_id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `Email`
  ADD CONSTRAINT `FKe_campaign_id` FOREIGN KEY (`campaign_id`) REFERENCES `Campaign` (`campaign_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FKe_email_blast_id` FOREIGN KEY (`email_blast_id`) REFERENCES `Email_Blast` (`email_blast_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FKe_subscriber_id` FOREIGN KEY (`subscriber_id`) REFERENCES `Subscriber` (`subscriber_id`) ON DELETE SET NULL ON UPDATE CASCADE;

ALTER TABLE `Email_Blast`
  ADD CONSTRAINT `FKeb_campaign_id` FOREIGN KEY (`campaign_id`) REFERENCES `Campaign` (`campaign_id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;
