/* Find all Emails by campaign_id */
SELECT *
  FROM Email e
  JOIN Campaign c ON c.campaign_id = e.campaign_id
  WHERE c.campaign_id = 1;


/* Find all Emails by subscriber_id */
SELECT *
  FROM Email e
  JOIN Subscriber s ON s.subscriber_id = e.subscriber_id
  WHERE s.subscriber_id = 1;


/* Find all opened Emails by campagin_id */
SELECT *
  FROM Email e
  JOIN Campaign c ON c.campaign_id = e.campaign_id
  WHERE c.campaign_id = 1 AND e.email_opened = true;


/* Find the number of opened emails by campaign_id */
SELECT COUNT(*)
  FROM Email e
  JOIN Campaign c ON c.campaign_id = e.campaign_id
  WHERE c.campaign_id = 1 AND e.email_opened = true;

/* Find all Emails with opened links by their campagin_id */
SELECT *
  FROM Email e
  JOIN Campaign c ON c.campaign_id = e.campaign_id
  WHERE c.campaign_id = 1 AND e.link_opened = true;


/* Find the number of opened emails by campaign_id */
SELECT COUNT(*)
  FROM Email e
  JOIN Campaign c ON c.campaign_id = e.campaign_id
  WHERE c.campaign_id = 1 AND e.link_opened = true;


/* Find all Campaigns by user_id */
SELECT *
  FROM Campaign c
  JOIN User u ON u.user_id = c.user_id
  WHERE u.user_id = 2;


/* Find all Campaigns by customer_id */
SELECT *
  FROM Campaign ca
  JOIN Customer cu ON cu.customer_id = ca.customer_id
  WHERE ca.customer_id = 1;


/* Find all Campaigns by Campaign_Type */
SELECT *
  FROM Campaign c
  WHERE c.campaign_id IN
  (
    SELECT ctc.campaign_id 
      FROM Campaign_Type_Campaign ctc
      JOIN Campaign_Type ct ON ct.campaign_type_id = ctc.campaign_type_id
      WHERE ct.name = 'Technology'
  );


/* Find all UNIQUE Subscribers by Subscriber_Groups  */
/*   v1   - This query uses the Campaign_Type enum to find results*/
SELECT DISTINCT *
  FROM Subscriber s
  WHERE s.subscriber_id IN
  (
    SELECT cts.subscriber_id
      FROM Campaign_Type_Subscriber cts
      WHERE cts.campaign_type_id IN
      (
        SELECT ct.campaign_type_id
          FROM Campaign_Type ct
          WHERE ct.name = "Sports"
      )
  );


/* Find all UNIQUE Subscribers by Campaign  */
SELECT DISTINCT *
    FROM Subscriber s
    WHERE s.subscriber_id IN
    (
      SELECT cts.subscriber_id
        FROM Campaign_Type_Subscriber cts
        WHERE cts.campaign_type_id IN
        (
          SELECT ct.campaign_type_id
            FROM Campaign_Type ct
            WHERE ct.campaign_type_id IN
            (
              SELECT ctc.campaign_type_id
                FROM Campaign_Type_Campaign ctc
                JOIN Campaign c ON c.campaign_id = ctc.campaign_id
                WHERE c.campaign_id = 1
            )
        )
    );

  
/*   v2   - This query uses the subscriber_group_id to find results*/
SELECT DISTINCT *
  FROM Subscriber s
  WHERE s.subscriber_id IN
  (
    SELECT cts.subscriber_id
      FROM Campaign_Type_Subscriber cts
      WHERE cts.campaign_type_id = 2 OR cts.campaign_type_id = 3
  );


  /* Alter the Campaign Table */
  ALTER TABLE Campaign
    ADD COLUMN `locked` boolean NOT NULL DEFAULT false,
    ADD COLUMN `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP;

  /* Alter the Email Table */
  ALTER TABLE Email
    ADD COLUMN `hash` CHAR(42) NOT NULL;

/* Insert email with hash */
INSERT INTO `Email` (`campaign_id`, `subscriber_id`) VALUES
(1, 1);


/* Get the statistics of an email blast */
SELECT (SELECT COUNT(*) FROM Email WHERE email_blast_id = 1) AS 'emails_sent',
       (SELECT COUNT(*) FROM Email WHERE email_blast_id = 1 AND email_opened = 1) AS 'emails_opened',
       (SELECT COUNT(*) FROM Email WHERE email_blast_id = 1 AND email_opened = 1) AS 'links_opened';


/* Get the total statistics of every email blast for a campaign */
SELECT (SELECT COUNT(*) FROM Email_Blast WHERE campaign_id = 2) AS 'blasts_sent', 
       (SELECT COUNT(*) FROM Email WHERE campaign_id = 2) AS 'emails_sent',
       (SELECT COUNT(*) FROM Email WHERE campaign_id = 2 AND email_opened = 1) AS 'emails_opened',
       (SELECT COUNT(*) FROM Email WHERE campaign_id = 2 AND email_opened = 1) AS 'links_opened';


/* Get the individual statistics for every email blast for a campaign */
SELECT eb.email_blast_id,
  (SELECT COUNT(*) FROM Email e WHERE campaign_id = 2 AND
    email_blast_id = eb.email_blast_id) AS 'emails_sent',
  (SELECT COUNT(*) FROM Email e2 WHERE campaign_id = 2 AND email_opened = 1 AND
    email_blast_id = eb.email_blast_id) AS 'emails_opened',
  (SELECT COUNT(*) FROM Email e3 WHERE campaign_id = 2 AND link_opened = 1 AND
    email_blast_id = eb.email_blast_id) AS 'links_opened'
  FROM Email_Blast eb
  WHERE eb.campaign_id = 2
  GROUP BY email_blast_id


/* Get Subscribers in a paginated way */
SELECT * FROM `Subscriber` LIMIT 5,10;


/* User Email Stats */
SELECT e.email_id, c.campaign_id,
  e.email_opened, e.link_opened FROM `Email` e
  JOIN `Campaign` c ON c.campaign_id = e.campaign_id
  WHERE c.user_id IN (SELECT u.user_id FROM `User` u WHERE user_id = 26)


/*  User Email Stats By ID */
SELECT u.email,
    COUNT(*) AS 'emails_sent',
    SUM(e.email_opened) AS 'emails_opened',
    SUM(e.link_opened) AS 'links_opened'
  FROM `Email` e
  JOIN `Campaign` c ON c.campaign_id = e.campaign_id
  JOIN `User` u ON u.user_id = c.user_id
  WHERE u.user_id = 26

/*  All Individual User Email Stats */
SELECT u.email,
    COUNT(*) AS 'emails_sent',
    SUM(e.email_opened) AS 'emails_opened',
    SUM(e.link_opened) AS 'links_opened'
  FROM `Email` e
  JOIN `Campaign` c ON c.campaign_id = e.campaign_id
  JOIN `User` u ON u.user_id = c.user_id
  GROUP BY u.email

/*  Sum of all User Email Stats */
SELECT 'all' AS 'email',
    COUNT(*) AS 'emails_sent',
    SUM(e.email_opened) AS 'emails_opened',
    SUM(e.link_opened) AS 'links_opened'
  FROM `Email` e
  JOIN `Campaign` c ON c.campaign_id = e.campaign_id
  JOIN `User` u ON u.user_id = c.user_id