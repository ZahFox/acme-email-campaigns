INSERT INTO `Customer` (`customer_id`, `name`) VALUES
    (1, 'Aperture Laboratories'),
    (2, 'Curiosity Shop'),
    (3, 'Gold Saucer');

INSERT INTO `Campaign_Type` (`campaign_type_id`, `name`) VALUES
    (1, 'Technology'),
    (2, 'Sports'),
    (3, 'Science');

DELIMITER //
DROP PROCEDURE IF EXISTS `bootstrap_database`//
CREATE DEFINER=`root`@`localhost` PROCEDURE `bootstrap_database`() NOT DETERMINISTIC MODIFIES SQL DATA SQL SECURITY INVOKER
  BEGIN

    INSERT INTO `Customer` (`customer_id`, `name`) VALUES
    (1, 'Aperture Laboratories'),
    (2, 'Curiosity Shop'),
    (3, 'Gold Saucer');

    INSERT INTO `Campaign_Type` (`campaign_type_id`, `name`) VALUES
    (1, 'Technology'),
    (2, 'Sports'),
    (3, 'Science');

    CALL `create_user`('admin@acme.com', '$2y$10$XwoobLoNpKHnquivlDWMyeNaTSEWAAN7RcWTR6S.dGE.l1LsOOXHi');
    CALL `create_user`('employee01@acme.com', '$2y$10$D1XzFa6G97G7lXGYkBgnIe4zyn.IYScdoOuw0UKCmyr9HA/4fLdMu');
    CALL `create_user`('employee02@acme.com', '$2y$10$/kWS6DsKWM.5.G9MYRgWSuQvO7kN6TSS2AU4BijTUDEhXs26Oqusa');
    CALL `create_campaign`('1', '2', 'Gravity Gun 2.0', 'Hello World!', 'http://http://half-life.wikia.com/wiki/Zero_Point_Energy_Field_Manipulator', '1,3,#');
    CALL `create_campaign`('2', '2', 'Big Bomb Bag Super Sale', 'Hello World!', 'https://zelda.gamepedia.com/Bomb_Bag', '2,#');
    CALL `create_campaign`('3', '3', 'Chocobo Racing 2017 World Series', 'Hello World!', 'http://www.finalfantasy.wikia.com/wiki/Chocobo_Square_(Final_Fantasy_VII)', '1,2,3,#');
    CALL `create_subscriber`('sample01@test.com', '1,2,#');
    CALL `create_subscriber`('sample02@test.com', '2,3,#');
    CALL `create_subscriber`('sample03@test.com', '3,#');
    CALL `create_subscriber`('sample04@test.com', '1,#');
    CALL `create_subscriber`('sample05@test.com', '1,2,3,#');
    CALL `create_campaign_email_blast`(1);
    CALL `create_campaign_email_blast`(2);
    CALL `create_campaign_email_blast`(3);

  END//
DELIMITER ;