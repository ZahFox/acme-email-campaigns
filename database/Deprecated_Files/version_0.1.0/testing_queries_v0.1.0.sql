/* Find all Emails by campaign_id */
SELECT *
  FROM Email e
  JOIN Campaign c ON c.campaign_id = e.campaign_id
  WHERE c.campaign_id = 1;


/* Find all Emails by subscriber_id */
SELECT *
  FROM Email e
  JOIN Subscriber s ON s.subscriber_id = e.subscriber_id
  WHERE s.subscriber_id = 1;


/* Find all opened Emails by campagin_id */
SELECT *
  FROM Email e
  JOIN Campaign c ON c.campaign_id = e.campaign_id
  WHERE c.campaign_id = 1 AND e.email_opened = true;


/* Find the number of opened emails by campaign_id */
SELECT COUNT(*)
  FROM Email e
  JOIN Campaign c ON c.campaign_id = e.campaign_id
  WHERE c.campaign_id = 1 AND e.email_opened = true;

/* Find all Emails with opened links by their campagin_id */
SELECT *
  FROM Email e
  JOIN Campaign c ON c.campaign_id = e.campaign_id
  WHERE c.campaign_id = 1 AND e.link_opened = true;


/* Find the number of opened emails by campaign_id */
SELECT COUNT(*)
  FROM Email e
  JOIN Campaign c ON c.campaign_id = e.campaign_id
  WHERE c.campaign_id = 1 AND e.link_opened = true;


/* Find all Campaigns by user_id */
SELECT *
  FROM Campaign c
  JOIN User u ON u.user_id = c.user_id
  WHERE u.user_id = 2;


/* Find all Campaigns by customer_id */
SELECT *
  FROM Campaign ca
  JOIN Customer cu ON cu.customer_id = ca.customer_id
  WHERE ca.customer_id = 1;


/* Find all Campaigns by Campaign_Type */
SELECT *
  FROM Campaign c
  WHERE c.campaign_id IN
  (
    SELECT ctc.campaign_id 
      FROM Campaign_Type_Campaign ctc
      JOIN Campaign_Type ct ON ct.campaign_type_id = ctc.campaign_type_id
      WHERE ct.name = 'Technology'
  );


/* Find all UNIQUE Subscribers by Subscriber_Groups  */
/*   v1   - This query uses the Campaign_Type enum to find results*/
SELECT DISTINCT *
  FROM Subscriber s
  WHERE s.subscriber_id IN
  (
    SELECT sgs.subscriber_id
      FROM Subscriber_Group_Subscriber sgs
      WHERE sgs.subscriber_group_id IN
      (
        SELECT sg.subscriber_group_id
          FROM Subscriber_Group sg
          JOIN Campaign_Type ct ON ct.campaign_type_id = sg.campaign_type_id
          WHERE ct.name = 'Sports' OR ct.name = 'Science'
      )
  );
/*   v2   - This query uses the subscriber_group_id to find results*/
SELECT DISTINCT *
  FROM Subscriber s
  WHERE s.subscriber_id IN
  (
    SELECT sgs.subscriber_id
      FROM Subscriber_Group_Subscriber sgs
      WHERE sgs.subscriber_group_id = 2 OR sgs.subscriber_group_id = 3
  );
