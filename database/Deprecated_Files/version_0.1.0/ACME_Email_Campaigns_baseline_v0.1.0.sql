-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 22, 2017 at 09:56 PM
-- Server version: 5.6.37
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `zahfoxco_ACME_Email_Campaigns`
--

-- --------------------------------------------------------

--
-- Table structure for table `Campaign`
--

CREATE TABLE `Campaign` (
  `campaign_id` int(10) UNSIGNED NOT NULL,
  `customer_id` smallint(5) UNSIGNED DEFAULT NULL,
  `user_id` smallint(5) UNSIGNED DEFAULT NULL,
  `title` varchar(50) NOT NULL,
  `content` varchar(10000) NOT NULL,
  `url` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Campaign_Type`
--

CREATE TABLE `Campaign_Type` (
  `campaign_type_id` smallint(5) UNSIGNED NOT NULL,
  `name` enum('Technology','Sports','Science') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Campaign_Type_Campaign`
--

CREATE TABLE `Campaign_Type_Campaign` (
  `campaign_id` int(11) UNSIGNED NOT NULL,
  `campaign_type_id` smallint(6) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Customer`
--

CREATE TABLE `Customer` (
  `customer_id` smallint(5) UNSIGNED NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Email`
--

CREATE TABLE `Email` (
  `email_id` int(10) UNSIGNED NOT NULL,
  `campaign_id` int(10) UNSIGNED DEFAULT NULL,
  `subscriber_id` int(10) UNSIGNED DEFAULT NULL,
  `time_sent` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `email_opened` tinyint(1) NOT NULL DEFAULT '0',
  `link_opened` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Subscriber`
--

CREATE TABLE `Subscriber` (
  `subscriber_id` int(10) UNSIGNED NOT NULL,
  `email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Subscriber_Group`
--

CREATE TABLE `Subscriber_Group` (
  `subscriber_group_id` smallint(5) UNSIGNED NOT NULL,
  `campaign_type_id` smallint(5) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Subscriber_Group_Subscriber`
--

CREATE TABLE `Subscriber_Group_Subscriber` (
  `subscriber_id` int(10) UNSIGNED NOT NULL,
  `subscriber_group_id` smallint(5) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `User`
--

CREATE TABLE `User` (
  `user_id` smallint(6) UNSIGNED NOT NULL,
  `user_type` enum('Admin','Employee') NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` char(64) NOT NULL,
  `salt` char(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Campaign`
--
ALTER TABLE `Campaign`
  ADD PRIMARY KEY (`campaign_id`),
  ADD KEY `customer_id` (`customer_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `Campaign_Type`
--
ALTER TABLE `Campaign_Type`
  ADD PRIMARY KEY (`campaign_type_id`),
  ADD UNIQUE KEY `unique_campaign_type` (`name`);

--
-- Indexes for table `Campaign_Type_Campaign`
--
ALTER TABLE `Campaign_Type_Campaign`
  ADD PRIMARY KEY (`campaign_id`,`campaign_type_id`),
  ADD KEY `campaign_type_id` (`campaign_type_id`);

--
-- Indexes for table `Customer`
--
ALTER TABLE `Customer`
  ADD PRIMARY KEY (`customer_id`),
  ADD UNIQUE KEY `unique_name` (`name`) USING BTREE;

--
-- Indexes for table `Email`
--
ALTER TABLE `Email`
  ADD PRIMARY KEY (`email_id`),
  ADD KEY `campaign_id` (`campaign_id`),
  ADD KEY `subscriber_id` (`subscriber_id`);

--
-- Indexes for table `Subscriber`
--
ALTER TABLE `Subscriber`
  ADD PRIMARY KEY (`subscriber_id`),
  ADD UNIQUE KEY `unique_subscriber_email` (`email`);

--
-- Indexes for table `Subscriber_Group`
--
ALTER TABLE `Subscriber_Group`
  ADD PRIMARY KEY (`subscriber_group_id`),
  ADD UNIQUE KEY `campaign_type_id_2` (`campaign_type_id`),
  ADD KEY `campaign_type_id` (`campaign_type_id`);

--
-- Indexes for table `Subscriber_Group_Subscriber`
--
ALTER TABLE `Subscriber_Group_Subscriber`
  ADD PRIMARY KEY (`subscriber_id`,`subscriber_group_id`),
  ADD KEY `subscriber_group_id` (`subscriber_group_id`);

--
-- Indexes for table `User`
--
ALTER TABLE `User`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `unique_email` (`email`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Campaign`
--
ALTER TABLE `Campaign`
  MODIFY `campaign_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `Campaign_Type`
--
ALTER TABLE `Campaign_Type`
  MODIFY `campaign_type_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `Customer`
--
ALTER TABLE `Customer`
  MODIFY `customer_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `Email`
--
ALTER TABLE `Email`
  MODIFY `email_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `Subscriber`
--
ALTER TABLE `Subscriber`
  MODIFY `subscriber_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `Subscriber_Group`
--
ALTER TABLE `Subscriber_Group`
  MODIFY `subscriber_group_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `User`
--
ALTER TABLE `User`
  MODIFY `user_id` smallint(6) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `Campaign`
--
ALTER TABLE `Campaign`
  ADD CONSTRAINT `Campaign_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `Customer` (`customer_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `Campaign_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `User` (`user_id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `Campaign_Type_Campaign`
--
ALTER TABLE `Campaign_Type_Campaign`
  ADD CONSTRAINT `Campaign_Type_Campaign_ibfk_1` FOREIGN KEY (`campaign_id`) REFERENCES `Campaign` (`campaign_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Campaign_Type_Campaign_ibfk_2` FOREIGN KEY (`campaign_type_id`) REFERENCES `Campaign_Type` (`campaign_type_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `Email`
--
ALTER TABLE `Email`
  ADD CONSTRAINT `Email_ibfk_1` FOREIGN KEY (`campaign_id`) REFERENCES `Campaign` (`campaign_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `Email_ibfk_2` FOREIGN KEY (`subscriber_id`) REFERENCES `Subscriber` (`subscriber_id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `Subscriber_Group`
--
ALTER TABLE `Subscriber_Group`
  ADD CONSTRAINT `Subscriber_Group_ibfk_1` FOREIGN KEY (`campaign_type_id`) REFERENCES `Campaign_Type` (`campaign_type_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `Subscriber_Group_Subscriber`
--
ALTER TABLE `Subscriber_Group_Subscriber`
  ADD CONSTRAINT `Subscriber_Group_Subscriber_ibfk_1` FOREIGN KEY (`subscriber_id`) REFERENCES `Subscriber` (`subscriber_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Subscriber_Group_Subscriber_ibfk_2` FOREIGN KEY (`subscriber_group_id`) REFERENCES `Subscriber_Group` (`subscriber_group_id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
