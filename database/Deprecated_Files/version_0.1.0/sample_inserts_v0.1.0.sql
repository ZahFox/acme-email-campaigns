/* ---------------------------------------------------------------------------------------------------------------------- */
/* The following queries may be used to populate the ACME_Email_Campaigns database with temporary sample data for testing */
/* ---------------------------------------------------------------------------------------------------------------------- */


/* Sample Users */
INSERT INTO `User` (`user_id`, `user_type`, `email`, `password`, `salt`) VALUES
(1, 'Admin', 'admin@acme.com', '405d013ca6522c62546a71eeeee75b4655d8f07e0f36c15b4145e9db035fcdb8', 'fHud32Ds7JiY'), /* Real Password : SHA2(salt + admin123 + salt, 256) */
(2, 'Employee', 'employee@acme.com', 'cb5c725fa83434afae6b231758018bcdaa0291ad4f1bc24d12b8da4ab92a80a8', 'd56Bna11Spq3'); /* Real Password : SHA2(salt + employee123 + salt, 256) */
/* ------------ */


/* Sample Campaign Types */
INSERT INTO `Campaign_Type` (`campaign_type_id`, `name`) VALUES
(1, 'Technology'),
(2, 'Sports'),
(3, 'Science');
/* -------------------- */


/* Sample Customers */
INSERT INTO `Customer` (`customer_id`, `name`) VALUES
(1, 'Aperture Laboratories'),
(2, 'Curiosity Shop'),
(3, 'Gold Saucer');
/* ---------------- */


/* Sample Subscribers */
INSERT INTO `Subscriber` (`subscriber_id`, `email`) VALUES
(1, 'samplesubscriber001@samples.com'),
(2, 'samplesubscriber002@samples.com'),
(3, 'samplesubscriber003@samples.com'),
(4, 'samplesubscriber004@samples.com'),
(5, 'samplesubscriber005@samples.com');
/* ------------------ */


/* Sample Campaigns */
INSERT INTO `Campaign` (`campaign_id`, `customer_id`, `user_id`, `title`, `content`, `url`) VALUES
(1, 1, 2, 'Gravity Gun 2.0', 'Hello World!', 'http://http://half-life.wikia.com/wiki/Zero_Point_Energy_Field_Manipulator'),
(2, 2, 2, 'Big Bomb Bag Super Sale', 'Hello World!', 'https://zelda.gamepedia.com/Bomb_Bag'),
(3, 3, 2, 'Chocobo Racing 2017 World Series', 'Hello World!', 'http://www.finalfantasy.wikia.com/wiki/Chocobo_Square_(Final_Fantasy_VII)');
/* ---------------- */


/* Sample Subscriber Groups */
INSERT INTO `Subscriber_Group` (`subscriber_group_id`, `campaign_type_id`) VALUES
(1, 1),
(2, 2),
(3, 3);
/* ------------------------ */


/* Sample Subscriber Group Subscribers */
INSERT INTO `Subscriber_Group_Subscriber` (`subscriber_id`, `subscriber_group_id`) VALUES
(1, 1),
(4, 1),
(5, 1),
(2, 2),
(4, 2),
(5, 2),
(3, 3),
(5, 3);
/* ----------------------------------- */


/* Sample Campaign Type Campaigns */
INSERT INTO `Campaign_Type_Campaign` (`campaign_id`, `campaign_type_id`) VALUES
(1, 1),
(2, 1),
(3, 2),
(1, 3);
/* ----------------------------- */


/* Sample Emails */
INSERT INTO `Email` (`email_id`, `campaign_id`, `subscriber_id`, `time_sent`, `email_opened`, `link_opened`) VALUES
(1, 1, 1, CURRENT_TIMESTAMP, 0, 0),
(2, 1, 1, CURRENT_TIMESTAMP, 0, 0),
(3, 1, 3, CURRENT_TIMESTAMP, 0, 0),
(4, 1, 3, CURRENT_TIMESTAMP, 0, 0),
(5, 1, 4, CURRENT_TIMESTAMP, 0, 0),
(6, 1, 4, CURRENT_TIMESTAMP, 0, 0),
(7, 1, 5, CURRENT_TIMESTAMP, 0, 0),
(8, 1, 5, CURRENT_TIMESTAMP, 0, 0),
(9, 2, 1, CURRENT_TIMESTAMP, 0, 0),
(10, 2, 1, CURRENT_TIMESTAMP, 0, 0),
(11, 2, 4, CURRENT_TIMESTAMP, 0, 0),
(12, 2, 4, CURRENT_TIMESTAMP, 0, 0),
(13, 2, 5, CURRENT_TIMESTAMP, 0, 0),
(14, 2, 5, CURRENT_TIMESTAMP, 0, 0),
(15, 3, 2, CURRENT_TIMESTAMP, 0, 0),
(16, 3, 2, CURRENT_TIMESTAMP, 0, 0),
(17, 3, 4, CURRENT_TIMESTAMP, 0, 0),
(18, 3, 4, CURRENT_TIMESTAMP, 0, 0),
(19, 3, 5, CURRENT_TIMESTAMP, 0, 0),
(20, 3, 5, CURRENT_TIMESTAMP, 0, 0);
/* ------------- */