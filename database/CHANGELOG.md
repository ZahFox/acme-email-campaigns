ACME Email Campaigns :: Database Changelog
==========================================

#### >> Current Version: v0.5.0

### Reference this document for changes to each version of the database ###

#### v0.1.0
* Created the [original](./Deprecated_Files/version_0.1.0/) database, SQL scripts, and ERD.

#### v0.2.0

* Deleted a table: - `Subscriber_Group`.
* Renamed a table:  `Subscriber_Group_Subscriber` ->  `Campaign_Type_Subscriber`;.
* Renamed a column on the `Campaign_Type_Subscriber` table: `subscriber_group_id` -> `campaign_type_id`;.
* Foreign Key Relationship added to the `Subscriber_Group` table: `campaign_type_id` -> `Campaign_Type.campaign_type_id`;.

#### v0.3.0
* Created 19 stored procedures for the database (one of them is for bootstrapping sample data into the database). For more information about these stored procedures please reference the [README file](./README.md) in this directory.
* Created a new table: + `Email_Blast`;. This table will be used to keep track of how many email blasts each Campaign creates.
* Updated the the `Campaign` table by adding two new columns: + `created`;  + `locked`;. created will be used to keep track of when each Campaign is created (it was also used to to make the create_campaign stored procedure more reliable). locked will be used to stop a Campaign from being updated after it has sent out atleast one email blast.
* Updated the `Email` table by removing one column and adding two new ones: + `hash`;  + `email_blast_id`;  - `time_sent`;. hash was added to make email tracking more secure. email_blast_id was added to track emails by each email blast instead of only each campaign. time_sent was removed because the Email_Blast table now holds that value.

#### v0.4.0
* Updated the procedure `create_campaign_email_blast`: The campaign that the email blast is associated with will now be set as locked if it isn't already.
* Updated the procedure `lock_campaign`: This procedure no longer returns any results.
* Updated the proceudres `open_email` and `open_link`: Now they both accept a subscriber_id and hash as parameters instead of an email_id. This was changed in order to work properly with the PHP email tracking.
* Created a new procedure: + `update_user_login`: This procedure may be used to update both the email and password for a User.
* Removed a column on the `User` table: - `salt`;.
* Updateed a column on the `User` table: `password` CHAR(64) -> CHAR(100);.
* Updated the procedure `create_user`: This procedure no longer creates a salt or hashes a User's password. PHP will do the hashing from here on out.
* Updated the procedure `update_user_login`: This procedure no longer creates a salt or hashes the new password.
* Updated the procedure `check_user_login`: This procedure now returns a User record based on the email parameter that is passed in.
* Updated the procedure `fetch_campaign`: This procedure now accpets `blast` as an option for the `i_type` parameter. This will return a Campaign record associated with an email_blast_id.
* Created a new procedure: + `fetch_email_blast_emails`: This procedure may be used to retrieve a record for each Email associated with an Email_Blast.

#### v0.5.0
* Created a new procedure: + `fetch_email_blast_stats`: This procedure may be retrieve the statistics of a particular `Email_Blast`. This includes how many actual emails were sent, how many of those emails were opened, and how many of links in the emails were clicked.
* Created a new procedure: + `fetch_campaign_blast_stats`: This procedure may be used to retrieve the total statistics of a every `Email_Blast` associated with a particular `Campaign`. This includes how many Email_Blasts this Campaign has, how many emails were sent, how many of those emails were opened, and how many of links in the emails were clicked.
* Created a new procedure: + `fetch_all_campaign_blast_stats`: This procedure may be retrieve individual statistics of a every `Email_Blast` associated with a particular `Campaign`. This includes how many Email_Blasts this Campaign has, how many emails were sent, how many of those emails were opened, and how many of links in the emails were clicked.
* Updated the procedure `update_campaign`: This procedure now accepts a types parameter so that the `Campaign_Type_Campaign` records may be updated, and a customer_id parameter so that the customer_id field may be updated.
* Created a new procedure: + `fetch_user`: This procedure may be used to retrieve a `User` record by passing in a user_id as a parameter.
* Created a new procedure: + `fetch_campaign_types_by_id`: This procedure may be used to retrieve `Campaign_Type` records associated with the campaign_id passed in as a parameter.
* Created a new procedure: + `fetch_customers`: This procedure may be used to retrieve all `Customer` records in the database.
* Created a new procedure: + `create_customer`: This procedure may be used to create a new `Customer` records in the database.
* Created a new procedure: + `create_campaign_type`: This procedure may be used to create a new `Campaign_Type` record in the database.

#### v0.6.0
* Updated the procedure `update_user_login`: This procedure no longer requires the `old_password` parameter. The back-end is now responsible for validating the password when updating a `User` record.
* Created a new procedure: + `fetch_users`: This procedure may be used to retrieve all `User` records in the database.
* Created a new procedure: + `fetch_subscribers`: This procedure may be used to retrieve `Subscriber` records in the database. These records are ordered by `subscriber_id`, and are paginated by the `i_start` and `i_max` parameters.
* Created a new procedure: + `delete_subscribers`: This procedure may be used to delete `Subscriber` records from the database.
* Created a new procedure: + `delete_user`: This procedure may be used to delete `User` records from the database.
* Created a new procedure: + `delete_customer`: This procedure may be used to delete `Customer` records from the database.
* Created a new procedure: + `update_customer`: This procedure may be used to update a `Customer` record in the database.