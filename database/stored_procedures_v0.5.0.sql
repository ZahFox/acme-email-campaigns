/*----------------------------------------------------------------------------------------------------------------------------------------------
/ * / ~~~~~~      CREATE CAMPAIGN      ~~~~~~ / * /  >> i_types { "#1,#2,#3..#n,#" } (numbers separated by commas ending with #)
*/----------------------------------------------------------------------------------------------------------------------------------------------
DELIMITER //
DROP PROCEDURE IF EXISTS `create_campaign`//

CREATE DEFINER=`root`@`localhost` PROCEDURE `create_campaign`(
  IN `i_customer_id` SMALLINT UNSIGNED, IN `i_user_id` SMALLINT UNSIGNED, IN `i_title` VARCHAR(50), IN `i_content` VARCHAR(10000),
  IN `i_url` VARCHAR(100), IN `i_types` VARCHAR(100)) NOT DETERMINISTIC MODIFIES SQL DATA SQL SECURITY DEFINER
  BEGIN
    DECLARE success BOOLEAN;
    DECLARE types_created SMALLINT;
    DECLARE rows_created SMALLINT;
    DECLARE n_campaign_id SMALLINT UNSIGNED;
    DECLARE time_created TIMESTAMP;
    DECLARE type_id SMALLINT;
    SET types_created = 0;
    SET autocommit = 0;
    SET success = 0;

    START TRANSACTION;
      SET time_created = NOW();
      INSERT INTO Campaign (`campaign_id`, `customer_id`, `user_id`, `title`, `content`, `url`, `created`) VALUES
      (NULL, i_customer_id, i_user_id, i_title, i_content, i_url, time_created);
      SET rows_created = ROW_COUNT();

      SET n_campaign_id = (SELECT c.campaign_id 
        FROM Campaign c
        WHERE c.created = time_created AND c.user_id = i_user_id AND c.title = i_title);

      WHILE (LOCATE('#', i_types) > 1)
      DO
        SET type_id = SUBSTRING( i_types, 1, (LOCATE(',', i_types) - 1) );
        SET i_types = SUBSTRING( i_types, (LOCATE(',', i_types) + 1) );
        INSERT INTO Campaign_Type_Campaign (`campaign_id`, `campaign_type_id`) VALUES(n_campaign_id, type_id);
        SET types_created = ROW_COUNT() + types_created;
      END WHILE;

      IF (types_created < 1 OR rows_created <> 1) THEN
        ROLLBACK;
      ELSE
        SET success = 1;
      END IF;
    COMMIT;

    SELECT success;

  END//
DELIMITER ;



/*----------------------------------------------------------------------------------------------------------------------------------------------
/ * / ~~~~~~      FETCH CAMPAIGN    ~~~~~~ / * / >> i_type { "campaign" || "customer" || "user" || "blast" || "all" }
*/----------------------------------------------------------------------------------------------------------------------------------------------
DELIMITER //
DROP PROCEDURE IF EXISTS `fetch_campaign`//

CREATE DEFINER=`root`@`localhost` PROCEDURE `fetch_campaign`(IN i_type VARCHAR(10), IN i_id INT UNSIGNED)
  NOT DETERMINISTIC CONTAINS SQL SQL SECURITY DEFINER
  BEGIN

    IF i_type = "campaign" THEN
      SELECT *
        FROM Campaign c
        WHERE c.campaign_id = i_id;
    ELSEIF i_type = "customer" THEN
      SELECT *
        FROM Campaign c
        WHERE c.customer_id = i_id;
    ELSEIF i_type = "user" THEN
      SELECT *
        FROM Campaign c
        WHERE c.user_id = i_id;
    ELSEIF i_type = "blast" THEN
      SELECT *
        FROM Campaign c
        WHERE c.campaign_id IN (
          SELECT eb.campaign_id
            FROM Email_Blast eb
            WHERE eb.email_blast_id = i_id
        );
    ELSEIF i_type = "all" THEN
      SELECT * FROM Campaign;
    END IF;

  END//
DELIMITER ;



/*----------------------------------------------------------------------------------------------------------------------------------------------
/ * / ~~~~~~      UPDATE CAMPAIGN    ~~~~~~ / * / >> pass in "" for { "i_title" || "i_content" || "i_url" } to ignore those fields
*/----------------------------------------------------------------------------------------------------------------------------------------------
DELIMITER //
DROP PROCEDURE IF EXISTS `update_campaign`//

CREATE DEFINER=`root`@`localhost` PROCEDURE `update_campaign`(IN i_id INT UNSIGNED, IN `i_customer_id` SMALLINT UNSIGNED,
  IN i_title VARCHAR(50), IN i_content VARCHAR(10000), IN i_url VARCHAR(100), IN `i_types` VARCHAR(100))
  NOT DETERMINISTIC CONTAINS SQL SQL SECURITY DEFINER
  BEGIN
    DECLARE success BOOLEAN;
    DECLARE is_locked BOOLEAN;
    DECLARE types_created SMALLINT;
    DECLARE rows_updated SMALLINT;
    DECLARE type_id SMALLINT;
    SET rows_updated = 0;
    SET autocommit = 0;
    SET success = 0;

    SET is_locked = (SELECT locked FROM Campaign WHERE campaign_id = i_id);
    IF is_locked <> TRUE THEN 
      START TRANSACTION;
        IF i_title <> "" THEN
          UPDATE Campaign SET title = i_title WHERE campaign_id = i_id;
          SET rows_updated = ROW_COUNT() + rows_updated;
        END IF;

        IF i_content <> "" THEN
          UPDATE Campaign SET content = i_content WHERE campaign_id = i_id;
          SET rows_updated = ROW_COUNT() + rows_updated;
        END IF;

        IF i_url <> "" THEN
          UPDATE Campaign SET url = i_url WHERE campaign_id = i_id;
          SET rows_updated = ROW_COUNT() + rows_updated;
        END IF;

        IF i_customer_id <> "" THEN
          UPDATE Campaign SET customer_id = i_customer_id WHERE campaign_id = i_id;
          SET rows_updated = ROW_COUNT() + rows_updated;
        END IF;

        IF i_types <> "" THEN
          DELETE FROM Campaign_Type_Campaign WHERE campaign_id = i_id;
          WHILE (LOCATE('#', i_types) > 1)
          DO
            SET type_id = SUBSTRING( i_types, 1, (LOCATE(',', i_types) - 1) );
            SET i_types = SUBSTRING( i_types, (LOCATE(',', i_types) + 1) );
            INSERT INTO Campaign_Type_Campaign (`campaign_id`, `campaign_type_id`) VALUES(i_id, type_id);
            SET types_created = ROW_COUNT() + types_created;
          END WHILE;

          SET rows_updated = ROW_COUNT() + rows_updated;
        END IF;

        IF rows_updated > 0 THEN
          SET success = 1;
        ELSE
          ROLLBACK;
        END IF;
      COMMIT;
    END IF;

    SELECT success;

  END//
DELIMITER ;



/*----------------------------------------------------------------------------------------------------------------------------------------------
/ * / ~~~~~~      DELETE CAMPAIGN    ~~~~~~ / * / >> Returns 0 for failure and 1 for success { CALL delete_campaign(6); }
*/----------------------------------------------------------------------------------------------------------------------------------------------
DELIMITER //
DROP PROCEDURE IF EXISTS `delete_campaign`//

CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_campaign`(IN i_id INT UNSIGNED)
NOT DETERMINISTIC CONTAINS SQL SQL SECURITY DEFINER
  BEGIN
    DECLARE success BOOLEAN;
    DECLARE rows_deleted SMALLINT;
    SET rows_deleted = 0;
    SET autocommit = 0;
    SET success = 0;
    
    START TRANSACTION;
      DELETE FROM Campaign WHERE campaign_id = i_id;
      SET rows_deleted = ROW_COUNT();
      IF rows_deleted > 0 THEN
        SET success = 1;
      ELSE
        ROLLBACK;
      END IF;
    COMMIT;
    
    SELECT success;

  END//
DELIMITER ;



/*----------------------------------------------------------------------------------------------------------------------------------------------
/ * / ~~~~~~      LOCK CAMPAIGN    ~~~~~~ / * /
*/----------------------------------------------------------------------------------------------------------------------------------------------
DELIMITER //
DROP PROCEDURE IF EXISTS `lock_campaign`//

CREATE DEFINER=`root`@`localhost` PROCEDURE `lock_campaign`(IN i_id INT UNSIGNED)
NOT DETERMINISTIC CONTAINS SQL SQL SECURITY DEFINER
  BEGIN
    DECLARE success BOOLEAN;
    DECLARE rows_updated SMALLINT;
    SET rows_updated = 0;
    SET autocommit = 0;
    SET success = 0;
    
    START TRANSACTION;
      UPDATE Campaign SET locked = 1 WHERE campaign_id = i_id;
      SET rows_updated = ROW_COUNT();
      IF rows_updated > 0 THEN
        SET success = 1;
      ELSE
        ROLLBACK;
      END IF;
    COMMIT;

  END//
DELIMITER ;



/*----------------------------------------------------------------------------------------------------------------------------------------------
/ * / ~~~~~~      CHECK CAMPAIGN LOCK    ~~~~~~ / * /
*/----------------------------------------------------------------------------------------------------------------------------------------------
DELIMITER //
DROP PROCEDURE IF EXISTS `check_campaign_lock`//

CREATE DEFINER=`root`@`localhost` PROCEDURE `check_campaign_lock`(IN i_id INT UNSIGNED)
NOT DETERMINISTIC CONTAINS SQL SQL SECURITY DEFINER
  BEGIN

    SELECT locked FROM Campaign WHERE campaign_id = i_id;

  END//
DELIMITER ;



/*----------------------------------------------------------------------------------------------------------------------------------------------
/ * / ~~~~~~      FETCH CAMPAIGN TYPES     ~~~~~~ / * /  
*/----------------------------------------------------------------------------------------------------------------------------------------------
DELIMITER //
DROP PROCEDURE IF EXISTS `fetch_campaign_types`//

CREATE DEFINER=`root`@`localhost` PROCEDURE `fetch_campaign_types`() NOT DETERMINISTIC CONTAINS SQL SQL SECURITY DEFINER
  BEGIN

    SELECT * FROM Campaign_Type;

  END//
DELIMITER ;



/*----------------------------------------------------------------------------------------------------------------------------------------------
/ * / ~~~~~~      FETCH CAMPAIGN TYPES BY ID     ~~~~~~ / * /  
*/----------------------------------------------------------------------------------------------------------------------------------------------
DELIMITER //
DROP PROCEDURE IF EXISTS `fetch_campaign_types_by_id`//

CREATE DEFINER=`root`@`localhost` PROCEDURE `fetch_campaign_types_by_id`(IN i_id INT UNSIGNED)
NOT DETERMINISTIC CONTAINS SQL SQL SECURITY DEFINER
  BEGIN

    SELECT * FROM Campaign_Type ct
      WHERE ct.campaign_type_id IN
      (
        SELECT ctc.campaign_type_id 
          FROM Campaign_Type_Campaign ctc
          WHERE ctc.campaign_id = i_id
      );

  END//
DELIMITER ;



/*----------------------------------------------------------------------------------------------------------------------------------------------
/ * / ~~~~~~      FETCH CAMPAIGN EMAIL BLASTS     ~~~~~~ / * /  
*/----------------------------------------------------------------------------------------------------------------------------------------------
DELIMITER //
DROP PROCEDURE IF EXISTS `fetch_email_campaign_blasts`//

CREATE DEFINER=`root`@`localhost` PROCEDURE `fetch_email_campaign_blasts`(IN i_id INT UNSIGNED)
NOT DETERMINISTIC CONTAINS SQL SQL SECURITY DEFINER
  BEGIN

    SELECT *
      FROM Email_Blast eb
      WHERE eb.campaign_id = i_id
      ORDER BY eb.time_sent DESC;

  END//
DELIMITER ;



/*----------------------------------------------------------------------------------------------------------------------------------------------
/ * / ~~~~~~      FETCH SUBSCRIBERS    ~~~~~~ / * /
*/----------------------------------------------------------------------------------------------------------------------------------------------
DELIMITER //
DROP PROCEDURE IF EXISTS `fetch_subscribers`//

CREATE DEFINER=`root`@`localhost` PROCEDURE `fetch_subscribers`(IN i_start INT UNSIGNED, IN i_max INT UNSIGNED)
NOT DETERMINISTIC CONTAINS SQL SQL SECURITY DEFINER
  BEGIN

    SELECT * FROM Subscriber ORDER BY subscriber_id LIMIT i_start,i_max;

  END//
DELIMITER ;



/*----------------------------------------------------------------------------------------------------------------------------------------------
/ * / ~~~~~~      FETCH CAMPAIGN SUBSCRIBERS     ~~~~~~ / * /  
*/----------------------------------------------------------------------------------------------------------------------------------------------
DELIMITER //
DROP PROCEDURE IF EXISTS `fetch_campaign_subscribers`//

CREATE DEFINER=`root`@`localhost` PROCEDURE `fetch_campaign_subscribers`(IN i_campaign_id INT UNSIGNED) NOT DETERMINISTIC CONTAINS SQL SQL SECURITY DEFINER
  BEGIN

    SELECT DISTINCT *
      FROM Subscriber s
      WHERE s.subscriber_id IN
      (
        SELECT cts.subscriber_id
          FROM Campaign_Type_Subscriber cts
          WHERE cts.campaign_type_id IN
          (
            SELECT ct.campaign_type_id
              FROM Campaign_Type ct
              WHERE ct.campaign_type_id IN
              (
                SELECT ctc.campaign_type_id
                  FROM Campaign_Type_Campaign ctc
                  JOIN Campaign c ON c.campaign_id = ctc.campaign_id
                  WHERE c.campaign_id = i_campaign_id
              )
          )
      );

  END//
DELIMITER ;



/*----------------------------------------------------------------------------------------------------------------------------------------------
/ * / ~~~~~~      DELETE SUBSCRIBER    ~~~~~~ / * /
*/----------------------------------------------------------------------------------------------------------------------------------------------
DELIMITER //
DROP PROCEDURE IF EXISTS `delete_subscriber`//

CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_subscriber`(IN i_id INT UNSIGNED)
NOT DETERMINISTIC CONTAINS SQL SQL SECURITY DEFINER
  BEGIN
    DECLARE success BOOLEAN;
    DECLARE rows_deleted SMALLINT;
    SET rows_deleted = 0;
    SET autocommit = 0;
    SET success = 0;
    
    START TRANSACTION;
      DELETE FROM Subscriber WHERE subscriber_id = i_id;
      SET rows_deleted = ROW_COUNT();
      IF rows_deleted > 0 THEN
        SET success = 1;
      ELSE
        ROLLBACK;
      END IF;
    COMMIT;

    SELECT success;

  END//
DELIMITER ;



/*----------------------------------------------------------------------------------------------------------------------------------------------
/ * / ~~~~~~      CREATE CAMPAIGN EMAIL BLAST     ~~~~~~ / * /  
*/----------------------------------------------------------------------------------------------------------------------------------------------
DELIMITER //
DROP PROCEDURE IF EXISTS `create_campaign_email_blast`//

CREATE DEFINER=`root`@`localhost` PROCEDURE `create_campaign_email_blast`(IN i_campaign_id INT UNSIGNED) NOT DETERMINISTIC CONTAINS SQL SQL SECURITY DEFINER
  BEGIN
    DECLARE e_success BOOLEAN;
    DECLARE e_blast_id INT;
    DECLARE fin INT DEFAULT 0;
    DECLARE s_id INT;
    DECLARE s_email VARCHAR(50);
    DECLARE time_created TIMESTAMP;

    DECLARE email_cursor CURSOR FOR
      SELECT DISTINCT *
        FROM Subscriber s
        WHERE s.subscriber_id IN
        (
          SELECT cts.subscriber_id
            FROM Campaign_Type_Subscriber cts
            WHERE cts.campaign_type_id IN
            (
              SELECT ct.campaign_type_id
                FROM Campaign_Type ct
                WHERE ct.campaign_type_id IN
                (
                  SELECT ctc.campaign_type_id
                    FROM Campaign_Type_Campaign ctc
                    JOIN Campaign c ON c.campaign_id = ctc.campaign_id
                    WHERE c.campaign_id = i_campaign_id
                )
            )
        );

      DECLARE CONTINUE HANDLER FOR NOT FOUND SET fin = 1;
      SET e_success = 0;
      SET autocommit = 0;
      SET time_created = NOW();
      START TRANSACTION;

        INSERT INTO Email_Blast (`email_blast_id`, `campaign_id`, `time_sent`) VALUES (NULL, i_campaign_id, time_created);
        SET e_blast_id = (SELECT eb.email_blast_id FROM Email_Blast eb WHERE eb.time_sent = time_created
          AND eb.campaign_id = i_campaign_id);
        CALL lock_campaign(i_campaign_id);
        
        OPEN email_cursor;
          get_email: LOOP

            FETCH email_cursor INTO s_id, s_email;

            IF fin = 1 THEN
              LEAVE get_email;
            END IF;

            CALL create_email(i_campaign_id, e_blast_id, s_id, time_created, e_success);
            IF e_success = 0 THEN
              ROLLBACK;
            END IF;

          END LOOP get_email;
        CLOSE email_cursor;
      COMMIT;

      IF e_success = 1 THEN
        SELECT s.subscriber_id, e.email_blast_id, s.email, e.hash
          FROM Email e
          JOIN Subscriber s ON s.subscriber_id = e.subscriber_id
          WHERE s.subscriber_id IN
          (
            SELECT DISTINCT s2.subscriber_id
                FROM Subscriber s2
                WHERE s2.subscriber_id IN
                (
                  SELECT cts.subscriber_id
                    FROM Campaign_Type_Subscriber cts
                    WHERE cts.campaign_type_id IN
                    (
                      SELECT ct.campaign_type_id
                        FROM Campaign_Type ct
                        WHERE ct.campaign_type_id IN
                        (
                          SELECT ctc.campaign_type_id
                            FROM Campaign_Type_Campaign ctc
                            JOIN Campaign c ON c.campaign_id = ctc.campaign_id
                            WHERE c.campaign_id = i_campaign_id
                        )
                    )
                )
          ) AND e.email_blast_id = e_blast_id;
      END IF;
      
  END//
DELIMITER ;



/*----------------------------------------------------------------------------------------------------------------------------------------------
/ * / ~~~~~~      FETCH EMAIL BLAST STATS     ~~~~~~ / * /  
*/----------------------------------------------------------------------------------------------------------------------------------------------
DELIMITER //
DROP PROCEDURE IF EXISTS `fetch_email_blast_stats`//

CREATE DEFINER=`root`@`localhost` PROCEDURE `fetch_email_blast_stats`(IN i_id INT UNSIGNED)
NOT DETERMINISTIC CONTAINS SQL SQL SECURITY DEFINER
  BEGIN

    SELECT (SELECT COUNT(*) FROM Email WHERE email_blast_id = i_id) AS 'emails_sent',
           (SELECT COUNT(*) FROM Email WHERE email_blast_id = i_id AND email_opened = 1) AS 'emails_opened',
           (SELECT COUNT(*) FROM Email WHERE email_blast_id = i_id AND email_opened = 1) AS 'links_opened';

  END//
DELIMITER ;



/*----------------------------------------------------------------------------------------------------------------------------------------------
/ * / ~~~~~~      FETCH CAMPAIGN BLAST STATS     ~~~~~~ / * /  
*/----------------------------------------------------------------------------------------------------------------------------------------------
DELIMITER //
DROP PROCEDURE IF EXISTS `fetch_campaign_blast_stats`//

CREATE DEFINER=`root`@`localhost` PROCEDURE `fetch_campaign_blast_stats`(IN i_id INT UNSIGNED)
NOT DETERMINISTIC CONTAINS SQL SQL SECURITY DEFINER
  BEGIN

SELECT (SELECT COUNT(*) FROM Email_Blast WHERE campaign_id = i_id) AS 'blasts_sent', 
       (SELECT COUNT(*) FROM Email WHERE campaign_id = i_id) AS 'emails_sent',
       (SELECT COUNT(*) FROM Email WHERE campaign_id = i_id AND email_opened = 1) AS 'emails_opened',
       (SELECT COUNT(*) FROM Email WHERE campaign_id = i_id AND link_opened = 1) AS 'links_opened';

  END//
DELIMITER ;



/*----------------------------------------------------------------------------------------------------------------------------------------------
/ * / ~~~~~~      FETCH ALL CAMPAIGN BLAST STATS     ~~~~~~ / * /  
*/----------------------------------------------------------------------------------------------------------------------------------------------
DELIMITER //
DROP PROCEDURE IF EXISTS `fetch_all_campaign_blast_stats`//

CREATE DEFINER=`root`@`localhost` PROCEDURE `fetch_all_campaign_blast_stats`(IN i_id INT UNSIGNED)
NOT DETERMINISTIC CONTAINS SQL SQL SECURITY DEFINER
  BEGIN

SELECT eb.email_blast_id, 
  (SELECT COUNT(*) FROM Email e WHERE campaign_id = i_id AND
    email_blast_id = eb.email_blast_id) AS 'emails_sent',
  (SELECT COUNT(*) FROM Email e2 WHERE campaign_id = i_id AND email_opened = 1 AND
    email_blast_id = eb.email_blast_id) AS 'emails_opened',
  (SELECT COUNT(*) FROM Email e3 WHERE campaign_id = i_id AND link_opened = 1 AND
    email_blast_id = eb.email_blast_id) AS 'links_opened'
  FROM Email_Blast eb
  WHERE eb.campaign_id = i_id
  GROUP BY email_blast_id;

  END//
DELIMITER ;



/*----------------------------------------------------------------------------------------------------------------------------------------------
/ * / ~~~~~~      CHECK EMAIL OPENED    ~~~~~~ / * /
*/----------------------------------------------------------------------------------------------------------------------------------------------
DELIMITER //
DROP PROCEDURE IF EXISTS `check_email_opened`//

CREATE DEFINER=`root`@`localhost` PROCEDURE `check_email_opened`(IN i_id INT UNSIGNED)
NOT DETERMINISTIC CONTAINS SQL SQL SECURITY DEFINER
  BEGIN

    SELECT email_opened FROM Email WHERE email_id = i_id;

  END//
DELIMITER ;



/*----------------------------------------------------------------------------------------------------------------------------------------------
/ * / ~~~~~~      CHECK LINK OPENED    ~~~~~~ / * /
*/----------------------------------------------------------------------------------------------------------------------------------------------
DELIMITER //
DROP PROCEDURE IF EXISTS `check_link_opened`//

CREATE DEFINER=`root`@`localhost` PROCEDURE `check_link_opened`(IN i_id INT UNSIGNED)
NOT DETERMINISTIC CONTAINS SQL SQL SECURITY DEFINER
  BEGIN

    SELECT link_opened FROM Email WHERE email_id = i_id;

  END//
DELIMITER ;



/*----------------------------------------------------------------------------------------------------------------------------------------------
/ * / ~~~~~~      OPEN EMAIL    ~~~~~~ / * /
*/----------------------------------------------------------------------------------------------------------------------------------------------
DELIMITER //
DROP PROCEDURE IF EXISTS `open_email`//

CREATE DEFINER=`root`@`localhost` PROCEDURE `open_email`(IN i_subscriber_id INT UNSIGNED, IN i_hash CHAR(42))
NOT DETERMINISTIC CONTAINS SQL SQL SECURITY DEFINER
  BEGIN
    DECLARE success BOOLEAN;
    DECLARE rows_updated SMALLINT UNSIGNED;
    SET autocommit = 0;
    SET success = 0;
    SET rows_updated = 0;

    START TRANSACTION;
      UPDATE Email SET `email_opened` = 1 WHERE subscriber_id = i_subscriber_id AND
          hash = i_hash;
      SET rows_updated = ROW_COUNT();
      IF rows_updated = 1 THEN
        SET success = 1;
      ELSE
        ROLLBACK;
      END IF;
    COMMIT;

    SELECT success;

  END//
DELIMITER ;



/*----------------------------------------------------------------------------------------------------------------------------------------------
/ * / ~~~~~~      OPEN LINK    ~~~~~~ / * /
*/----------------------------------------------------------------------------------------------------------------------------------------------
DELIMITER //
DROP PROCEDURE IF EXISTS `open_link`//

CREATE DEFINER=`root`@`localhost` PROCEDURE `open_link`(IN i_subscriber_id INT UNSIGNED, IN i_hash CHAR(42))
NOT DETERMINISTIC CONTAINS SQL SQL SECURITY DEFINER
  BEGIN
    DECLARE success BOOLEAN;
    DECLARE rows_updated SMALLINT UNSIGNED;
    SET autocommit = 0;
    SET success = 0;
    SET rows_updated = 0;

    START TRANSACTION;
      UPDATE Email SET `link_opened` = 1 WHERE subscriber_id = i_subscriber_id AND
          hash = i_hash;
      SET rows_updated = ROW_COUNT();
      IF rows_updated = 1 THEN
        SET success = 1;
      ELSE
        ROLLBACK;
      END IF;
    COMMIT;

    SELECT success;

  END//
DELIMITER ;



/*----------------------------------------------------------------------------------------------------------------------------------------------
/ * / ~~~~~~      CREATE EMAIL    ~~~~~~ / * /
*/----------------------------------------------------------------------------------------------------------------------------------------------
DELIMITER //
DROP PROCEDURE IF EXISTS `create_email`//

CREATE DEFINER=`root`@`localhost` PROCEDURE `create_email`(IN i_campaign_id INT UNSIGNED, IN i_email_blast_id INT UNSIGNED,
IN i_subscriber_id INT UNSIGNED, IN i_time_sent TIMESTAMP, OUT success BOOLEAN)
NOT DETERMINISTIC CONTAINS SQL SQL SECURITY DEFINER
  BEGIN
    DECLARE rows_updated SMALLINT;
    DECLARE random INT;
    DECLARE salt CHAR(32);
    DECLARE new_hash CHAR(42);
    SET rows_updated = 0;
    SET autocommit = 0;
    SET success = 0;
    SET random = FLOOR( 1 + (RAND() * 343 ) );
    SET salt = CAST(random AS CHAR(32));
    SET new_hash = SHA1(CONCAT(salt, i_campaign_id, i_subscriber_id, i_time_sent, salt));
    
    START TRANSACTION;
      INSERT INTO Email (`campaign_id`, `subscriber_id`, `hash`, `email_blast_id`) VALUES
        (i_campaign_id, i_subscriber_id, new_hash, i_email_blast_id);
      SET rows_updated = ROW_COUNT();
      IF rows_updated > 0 THEN
        SET success = 1;
      ELSE
        ROLLBACK;
      END IF;
    COMMIT;

  END//
DELIMITER ;



/*----------------------------------------------------------------------------------------------------------------------------------------------
/ * / ~~~~~~      FETCH EMAIL BLAST EMAILS    ~~~~~~ / * /
*/----------------------------------------------------------------------------------------------------------------------------------------------
DELIMITER //
DROP PROCEDURE IF EXISTS `fetch_email_blast_emails`//

CREATE DEFINER=`root`@`localhost` PROCEDURE `fetch_email_blast_emails`(IN i_id INT UNSIGNED)
NOT DETERMINISTIC CONTAINS SQL SQL SECURITY DEFINER
  BEGIN
    SELECT e.email_id, e.campaign_id, e.subscriber_id, e.email_opened,
      e.link_opened, e.email_blast_id
      FROM Email e
      WHERE e.email_blast_id = i_id;
  END//
DELIMITER ;



/*----------------------------------------------------------------------------------------------------------------------------------------------
/ * / ~~~~~~      CREATE SUBSCRIBER      ~~~~~~ / * /  >> i_types { "1,2,3..n,#" } (numbers separated by commas ending with #)
*/----------------------------------------------------------------------------------------------------------------------------------------------
DELIMITER //
DROP PROCEDURE IF EXISTS `create_subscriber`//

CREATE DEFINER=`root`@`localhost` PROCEDURE `create_subscriber`(IN `i_email` VARCHAR(50), IN `i_types` VARCHAR(100))
NOT DETERMINISTIC MODIFIES SQL DATA SQL SECURITY DEFINER
  BEGIN
    DECLARE success BOOLEAN;
    DECLARE types_created SMALLINT;
    DECLARE rows_created SMALLINT;
    DECLARE s_id SMALLINT;
    DECLARE type_id SMALLINT;
    SET types_created = 0;
    SET autocommit = 0;
    SET success = 0;

    START TRANSACTION;
      INSERT INTO Subscriber (`subscriber_id`, `email`) VALUES (NULL, i_email);
      SET rows_created = ROW_COUNT();

      SET s_id = (SELECT s.subscriber_id 
        FROM Subscriber s
        WHERE s.email = i_email);

      WHILE (LOCATE('#', i_types) > 1)
      DO
        SET type_id = SUBSTRING( i_types, 1, (LOCATE(',', i_types) - 1) );
        SET i_types = SUBSTRING( i_types, (LOCATE(',', i_types) + 1) );
        INSERT INTO Campaign_Type_Subscriber (`subscriber_id`, `campaign_type_id`) VALUES(s_id, type_id);
        SET types_created = ROW_COUNT() + types_created;
      END WHILE;

      IF (types_created < 1 OR rows_created <> 1) THEN
        ROLLBACK;
      ELSE
        SET success = 1;
      END IF;
    COMMIT;

    SELECT success;

  END//
DELIMITER ;



/*----------------------------------------------------------------------------------------------------------------------------------------------
/ * / ~~~~~~      CHECK USER LOGIN    ~~~~~~ / * /
*/----------------------------------------------------------------------------------------------------------------------------------------------
DELIMITER //
DROP PROCEDURE IF EXISTS `check_user_login`//

CREATE DEFINER=`root`@`localhost` PROCEDURE `check_user_login`(IN i_email VARCHAR(50))
NOT DETERMINISTIC CONTAINS SQL SQL SECURITY DEFINER
  BEGIN

    SELECT * FROM User u WHERE u.email = i_email;

  END//
DELIMITER ;



/*----------------------------------------------------------------------------------------------------------------------------------------------
/ * / ~~~~~~      UPDATE USER LOGIN    ~~~~~~ / * /
*/----------------------------------------------------------------------------------------------------------------------------------------------
DELIMITER //
DROP PROCEDURE IF EXISTS `update_user_login`//

CREATE DEFINER=`root`@`localhost` PROCEDURE `update_user_login`(IN i_id SMALLINT UNSIGNED, IN i_old_email VARCHAR(50),
IN i_new_email VARCHAR(50), IN i_new_password VARCHAR(100)) NOT DETERMINISTIC CONTAINS SQL SQL SECURITY DEFINER
  BEGIN
    DECLARE success BOOLEAN;
    DECLARE valid BOOLEAN;
    DECLARE rows_updated SMALLINT;
    SET rows_updated = 0;
    SET autocommit = 0;
    SET success = 0;
    SET valid = 0;

    SET valid = (SELECT COUNT(*)
                  FROM User u
                  WHERE u.email = i_old_email);

    IF valid = 1 THEN
      START TRANSACTION;
        UPDATE User u SET u.email = i_new_email, u.password = i_new_password
          WHERE u.user_id = i_id;
        SET rows_updated = ROW_COUNT();

        IF (rows_updated <> 1) THEN
          ROLLBACK;
        ELSE
          SET success = 1;
        END IF;
      COMMIT;
    END IF;
    
    SELECT success;

  END//
DELIMITER ;



/*----------------------------------------------------------------------------------------------------------------------------------------------
/ * / ~~~~~~      CREATE USER    ~~~~~~ / * /
*/----------------------------------------------------------------------------------------------------------------------------------------------
DELIMITER //
DROP PROCEDURE IF EXISTS `create_user`//

CREATE DEFINER=`root`@`localhost` PROCEDURE `create_user`(IN i_email VARCHAR(50), IN i_password CHAR(60))
NOT DETERMINISTIC CONTAINS SQL SQL SECURITY DEFINER
  BEGIN
    DECLARE success BOOLEAN;
    DECLARE rows_created SMALLINT;

    SET rows_created = 0;
    SET autocommit = 0;
    SET success = 0;

    START TRANSACTION;
      INSERT INTO User (`user_type`, `email`, `password`) VALUES('Employee', i_email, i_password);
      SET rows_created = ROW_COUNT();

      IF (rows_created <> 1) THEN
        ROLLBACK;
      ELSE
        SET success = 1;
      END IF;
    COMMIT;

    SELECT success;

  END//
DELIMITER ;



/*----------------------------------------------------------------------------------------------------------------------------------------------
/ * / ~~~~~~      FETCH USER    ~~~~~~ / * /
*/----------------------------------------------------------------------------------------------------------------------------------------------
DELIMITER //
DROP PROCEDURE IF EXISTS `fetch_user`//

CREATE DEFINER=`root`@`localhost` PROCEDURE `fetch_user`(IN i_id INT UNSIGNED)
NOT DETERMINISTIC CONTAINS SQL SQL SECURITY DEFINER
  BEGIN

    SELECT u.user_type, u.email
      FROM User u
      WHERE u.user_id = i_id;

  END//
DELIMITER ;



/*----------------------------------------------------------------------------------------------------------------------------------------------
/ * / ~~~~~~      FETCH USERS    ~~~~~~ / * /
*/----------------------------------------------------------------------------------------------------------------------------------------------
DELIMITER //
DROP PROCEDURE IF EXISTS `fetch_users`//

CREATE DEFINER=`root`@`localhost` PROCEDURE `fetch_users`()
NOT DETERMINISTIC CONTAINS SQL SQL SECURITY DEFINER
  BEGIN

    SELECT u.user_type, u.email
      FROM User u;

  END//
DELIMITER ;



/*----------------------------------------------------------------------------------------------------------------------------------------------
/ * / ~~~~~~      FETCH CUSTOMERS    ~~~~~~ / * /
*/----------------------------------------------------------------------------------------------------------------------------------------------
DELIMITER //
DROP PROCEDURE IF EXISTS `fetch_customers`//

CREATE DEFINER=`root`@`localhost` PROCEDURE `fetch_customers`()
NOT DETERMINISTIC CONTAINS SQL SQL SECURITY DEFINER
  BEGIN

    SELECT * FROM Customer;

  END//
DELIMITER ;



/*----------------------------------------------------------------------------------------------------------------------------------------------
/ * / ~~~~~~      DELETE USER    ~~~~~~ / * /
*/----------------------------------------------------------------------------------------------------------------------------------------------
DELIMITER //
DROP PROCEDURE IF EXISTS `delete_user`//

CREATE DEFINER=`cpses_zaml6gyd5r`@`localhost` PROCEDURE `delete_user`(IN i_email VARCHAR(50))
NOT DETERMINISTIC CONTAINS SQL SQL SECURITY DEFINER
  BEGIN
    DECLARE success BOOLEAN;
    DECLARE rows_deleted SMALLINT;
    SET rows_deleted = 0;
    SET autocommit = 0;
    SET success = 0;
    
    START TRANSACTION;
      DELETE FROM User WHERE `email` = i_email;
      SET rows_deleted = ROW_COUNT();
      IF rows_deleted > 0 THEN
        SET success = 1;
      ELSE
        ROLLBACK;
      END IF;
    COMMIT;


    SELECT success;

  END//
DELIMITER ;



/*----------------------------------------------------------------------------------------------------------------------------------------------
/ * / ~~~~~~      CREATE CUSTOMER    ~~~~~~ / * /
*/----------------------------------------------------------------------------------------------------------------------------------------------
DELIMITER //
DROP PROCEDURE IF EXISTS `create_customer`//

CREATE DEFINER=`root`@`localhost` PROCEDURE `create_customer`(IN i_name VARCHAR(50))
NOT DETERMINISTIC CONTAINS SQL SQL SECURITY DEFINER
  BEGIN
      DECLARE success BOOLEAN;
      DECLARE rows_created SMALLINT;
      SET rows_created = 0;
      SET success = 0;

      START TRANSACTION;
      INSERT INTO `Customer` (`name`) VALUES (i_name);
      SET rows_created = ROW_COUNT();

      IF (rows_created <> 1) THEN
        ROLLBACK;
      ELSE
        SET success = 1;
      END IF;
    COMMIT;

    SELECT success;

  END//
DELIMITER ;



/*----------------------------------------------------------------------------------------------------------------------------------------------
/ * / ~~~~~~      DELETE CUSTOMER    ~~~~~~ / * /
*/----------------------------------------------------------------------------------------------------------------------------------------------
DELIMITER //
DROP PROCEDURE IF EXISTS `delete_customer`//

CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_customer`(IN i_id INT UNSIGNED)
NOT DETERMINISTIC CONTAINS SQL SQL SECURITY DEFINER
  BEGIN
    DECLARE success BOOLEAN;
    DECLARE rows_deleted SMALLINT;
    SET rows_deleted = 0;
    SET autocommit = 0;
    SET success = 0;
    
    START TRANSACTION;
      DELETE FROM Customer WHERE `customer_id` = i_id;
      SET rows_deleted = ROW_COUNT();
      IF rows_deleted > 0 THEN
        SET success = 1;
      ELSE
        ROLLBACK;
      END IF;
    COMMIT;

    SELECT success;

  END//
DELIMITER ;



/*----------------------------------------------------------------------------------------------------------------------------------------------
/ * / ~~~~~~      UPDATE CUSTOMER    ~~~~~~ / * /
*/----------------------------------------------------------------------------------------------------------------------------------------------
DELIMITER //
DROP PROCEDURE IF EXISTS `update_customer`//

CREATE DEFINER=`root`@`localhost` PROCEDURE `update_customer`(IN i_id INT UNSIGNED, IN i_name VARCHAR(50))
NOT DETERMINISTIC CONTAINS SQL SQL SECURITY DEFINER
  BEGIN
    DECLARE success BOOLEAN;
    DECLARE rows_updated SMALLINT;
    SET rows_updated = 0;
    SET autocommit = 0;
    SET success = 0;
    
    START TRANSACTION;
      UPDATE Customer SET `name` = i_name WHERE `customer_id` = i_id;
      SET rows_updated = ROW_COUNT();
      IF (rows_updated <> 1) THEN
        ROLLBACK;
      ELSE
        SET success = 1;
      END IF;
    COMMIT;

    SELECT success;

  END//
DELIMITER ;



/*----------------------------------------------------------------------------------------------------------------------------------------------
/ * / ~~~~~~      CREATE CAMPAIGN TYPE    ~~~~~~ / * /
*/----------------------------------------------------------------------------------------------------------------------------------------------
DELIMITER //
DROP PROCEDURE IF EXISTS `create_campaign_type`//

CREATE DEFINER=`root`@`localhost` PROCEDURE `create_campaign_type`(IN i_name VARCHAR(50))
NOT DETERMINISTIC CONTAINS SQL SQL SECURITY DEFINER
  BEGIN
    DECLARE success BOOLEAN;
    DECLARE rows_created SMALLINT;
    SET rows_created = 0;
    SET success = 0;

    START TRANSACTION;
      INSERT INTO `Campaign_Type` (`name`) VALUES (i_name);
      SET rows_created = ROW_COUNT();

      IF (rows_created <> 1) THEN
        ROLLBACK;
      ELSE
        SET success = 1;
      END IF;
    COMMIT;

    SELECT success;

  END//
DELIMITER ;